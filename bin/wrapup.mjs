#!/usr/bin/env zx --quiet
// INSTALL:
//  npm install zx --global

async function countCommits(since, until) {
  const users = (await $`git log --format='%an' | sort -u`).toString()
    .split('\n')
    .filter((user) => user !== '');

  const longestUsernameLength = Math.max(...users.map(user => user.length));

  echo(`Commit Count Report`);
  echo(`Since: ${since}`);
  echo(`Until: ${until}`);
  echo('------------------------');

  let totalCount = 0;

  for (const user of users) {
    const count = (await $`git log --since="${since}" --until="${until}" --author=${user} --pretty=format:"" | wc -l`).toString().trim();
    const padding = ' '.repeat(longestUsernameLength - user.length + 2);
    echo(`${user}${padding}${count}`);
    totalCount += parseInt(count);
  }

  const padding = ' '.repeat(longestUsernameLength - "TOTAL".length + 2);
  echo(`TOTAL${padding}${totalCount}`);
}

async function calculateCommitsPerYear(sinceYear) {
  const currentYear = new Date().getFullYear();
  const years = Array.from({length: currentYear - sinceYear + 1}, (v, k) => k + parseInt(sinceYear));

  for (const year of years) {
    const since = `${year}-01-01`;
    const until = `${year}-12-31`;

    const count = (await $`git log --since="${since}" --until="${until}" --pretty=format:"" | wc -l`).toString().trim();
    echo(`Year: ${year} (${count} commits)`)
  }

}

await calculateCommitsPerYear("2018");
echo("")
await countCommits("2023-01-01", "2023-12-31")
