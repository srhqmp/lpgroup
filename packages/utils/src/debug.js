/* eslint-disable no-prototype-builtins */
/* eslint-disable import/no-named-default */
/* eslint-disable no-console */
import debugRoot from "debug";
import { isObjectLike } from "lodash-es";
import { filterDeepObject } from "./object.js";
import { removeCircularReferences } from "./general.js";

function write(output) {
  return (...args) => {
    const msg = [];
    args.forEach((arg) => {
      const cleanArg = removeCircularReferences(arg);
      if (isObjectLike(cleanArg)) {
        const keys = ["uri"];
        msg.push(
          JSON.stringify(filterDeepObject(cleanArg, keys, "<removed by debug>"), null, "  "),
        );
      } else {
        msg.push(cleanArg);
      }
    });
    output(msg.join(" "));
  };
}

function info(name, namespace) {
  const dbg = debugRoot(`${namespace}:${name}:info`);
  dbg.log = write(console.log);
  return dbg;
}

function warning(name, namespace) {
  const dbg = debugRoot(`${namespace}:${name}:warning`);
  dbg.log = write(console.error);
  return dbg;
}

function error(name, namespace) {
  const dbg = debugRoot(`${namespace}:${name}:error`);
  dbg.log = write(console.error);
  return dbg;
}

function debug(name, namespace) {
  const dbg = debugRoot(`${namespace}:${name}:debug`);
  dbg.log = write(console.log);
  return dbg;
}

// Cached debug functions for used namespaces
const debugs = {};

export function log(name, namespace = "lpgroup") {
  const key = `${namespace}:${name}`;
  if (!debugs[key])
    debugs[key] = {
      info: info(name, namespace),
      warning: warning(name, namespace),
      error: error(name, namespace),
      debug: debug(name, namespace),
    };

  return debugs[key];
}
