import sanitize from "sanitize-filename";
import { isObjectLike, isPlainObject, isFunction } from "lodash-es";

/**
 * Format a valid brandsign filename.
 * ie. remove / and make sure .pdf is the extension.
 */
export function formatFileName(name, ext = ".pdf") {
  return sanitize(name, { replacement: "_" }) + (name.includes(ext) ? "" : ext);
}

export function removeCircularReferences(obj) {
  const seen = new WeakSet();

  function replacer(key, value) {
    if (value === undefined) {
      return "<undefined>"; // remove circular reference
    }
    if (isFunction(value)) {
      return "<function>"; // remove circular reference
    }

    if (isObjectLike(value) && value !== null) {
      if (seen.has(value)) {
        return "<circular>"; // remove circular reference
      }
      seen.add(value);

      if (Array.isArray(value)) {
        // Handle arrays
        return value.map((item) => replacer(null, item));
      }

      if (isPlainObject(value)) {
        // Handle objects
        const newObj = {};
        // eslint-disable-next-line no-restricted-syntax, guard-for-in
        for (const prop in value) {
          newObj[prop] = replacer(prop, value[prop]);
        }
        return newObj;
      }
    }
    return value;
  }

  return JSON.parse(JSON.stringify(obj, replacer));
}

/**
 * Executes a provided callback function sequentially on each item in the array,
 * and accumulates the results into a new array.
 *
 * @param {Array} array - The array to iterate over.
 * @param {Function} callback - The callback function to execute on each item.
 *                               The callback function should return a Promise.
 * @returns {Promise<Array>} A Promise that resolves to an array of results.
 */
export async function executeSequentially(array, callback) {
  return array.reduce(async (promiseAccumulator, currentItem) => {
    // Wait for the accumulated Promise to resolve
    const accumulator = await promiseAccumulator;

    // Execute the callback on the current item and wait for it to resolve
    const result = await callback(currentItem);

    // Return a new array that includes all previous results and the current result
    return [...accumulator, result];
  }, Promise.resolve([])); // Start with a Promise resolving to an empty array
}

/**
 * Executes a provided callback function in parallel on each item in the array,
 * and accumulates the results into a new array.
 *
 * @param {Array} array - The array to iterate over.
 * @param {Function} callback - The callback function to execute on each item.
 *                               The callback function should return a Promise.
 * @returns {Promise<Array>} A Promise that resolves to an array of results.
 */
export async function executeInParallel(array, callback) {
  return Promise.all(array.map(callback));
}
