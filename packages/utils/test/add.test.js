import { add } from "../src/index";

describe("add", () => {
  test("Golden path", async () => {
    expect(add(12, 12)).toEqual(24);
  });
});
