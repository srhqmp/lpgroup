import { log } from "../src/index";

const { debug } = log("unittest");

describe("debug log", () => {
  test("Golden path", async () => {
    expect(debug("Unit Test")).toEqual(undefined);
  });
});
