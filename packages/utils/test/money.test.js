import { money } from "../src/index";

describe("money", () => {
  test("Golden path", async () => {
    expect(money(12)).toEqual("mo 12");
  });
});
