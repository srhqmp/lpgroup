import { validReq } from "@lpgroup/yup";
import { disallow } from "feathers-hooks-common";

export default {
  before: {
    all: [],
    find: [disallow()],
    get: [disallow()],
    create: [validReq()],
    update: [disallow()],
    patch: [disallow()],
    remove: [disallow()],
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
