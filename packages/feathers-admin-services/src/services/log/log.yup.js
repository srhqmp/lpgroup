import * as cy from "@lpgroup/yup";

const requestSchema = {
  type: cy
    .shortText()
    .matches(/info|error|warning|debug/)
    .default("info"),
  message: cy.shortText().defaultNull(),
};

export default cy.buildValidationSchema(requestSchema);
