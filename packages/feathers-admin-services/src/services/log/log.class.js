import { log } from "@lpgroup/utils";

const { info, error, warning, debug } = log("app");

export class Log {
  constructor(options, app) {
    this.options = options || {};
    this.app = app;
  }

  // eslint-disable-next-line no-unused-vars
  async create(data, params) {
    if (data.type === "info") info(data.message);
    else if (data.type === "error") error(data.message);
    else if (data.type === "warning") warning(data.message);
    else if (data.type === "debug") debug(data.message);
    return { code: 200, status: "OK" };
  }
}
