import { Log } from "./log.class.js";
import hooks from "./log.hooks.js";
import schema from "./log.yup.js";

export default (app) => {
  const options = { schema };
  app.use("/admin/log", new Log(options, app));
  const service = app.service("admin/log");
  service.hooks(hooks);
};
