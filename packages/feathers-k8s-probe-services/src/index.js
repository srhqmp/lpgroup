import healthy from "./services/healthy/healthy.service.js";
import ready from "./services/ready/ready.service.js";

export { default as healthy } from "./services/healthy/healthy.service.js";
export { default as ready } from "./services/ready/ready.service.js";

// Configure
export default (app) => {
  app.configure(healthy);
  app.configure(ready);
};
