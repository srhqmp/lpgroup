import { getItems } from "feathers-hooks-common";

export function buildItemHook(cb) {
  return async (context) => {
    const items = getItems(context);
    if (items) {
      const arrItems = Array.isArray(items) ? items : [items];
      await Promise.all(
        arrItems.map(async (item) => {
          return cb(context, item);
        }),
      );
    }

    return context;
  };
}

/**
 * Create a fethersjs params object to use on internal calls.
 */
export function internalParams(params, query = {}, options = {}) {
  const newParam = { ...params, query, provider: "" };
  if (options.clearSession) {
    if (newParam.mongodb) delete newParam.mongodb;
    if (newParam.sessionId) delete newParam.sessionId;
  }
  return newParam;
}

export function sleep(ms) {
  return new Promise((resolve) => {
    setTimeout(resolve, ms);
  });
}

export function getRandomInt(max) {
  return Math.random() * Math.floor(max);
}

export function getExponentialTimeoutWithJitter(
  retry,
  writeConflictJitterMax = 100,
  writeConflictBase = 50,
) {
  const exp = getRandomInt(retry * 0.5) + retry * 0.5;
  const exponent = 1.6 ** exp;
  const rand = getRandomInt(writeConflictJitterMax * retry);
  return Math.floor(writeConflictBase * exponent + rand);
}
