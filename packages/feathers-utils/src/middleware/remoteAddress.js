/* eslint-disable no-param-reassign */
/* eslint-disable no-unused-vars */
import { log } from "@lpgroup/utils";

const { debug } = log("middleware");

/**
 * Middleware to obtain the remote address (IP) of the client.
 * Works for both WebSocket
 *
 * @param {object} [socket] - The socket instance
 * @returns {function} - The middleware function.
 */
export function ioRemoteAddress(socket) {
  const ip =
    socket?.request?.headers["x-forwarded-for"] ||
    socket?.request?.headers["x-real-ip"] ||
    socket?.request?.connection?.remoteAddress ||
    socket.feathers.remoteAddress;
  if (ip) {
    socket.feathers.remoteAddress = ip;
  }
  debug({
    type: "socket",
    ip,
    feathersRemoteAddress: socket.feathers.remoteAddress,
    connectionRemoteAddress: socket?.request?.connection?.remoteAddress,
    headers: socket?.request?.headers,
  });
}

/**
 * Middleware to obtain the remote address (IP) of the client.
 * Works for both REST service.
 *
 * @param {object} app - The application instance.
 * @returns {function} - The middleware function.
 */
export function restRemoteAddress(app) {
  return (req, res, next) => {
    const ip = req.headers["x-forwarded-for"] || req.ip || req.socket.remoteAddress;
    req.feathers.remoteAddress = ip;
    debug({
      type: "rest",
      ip,
      headers: req.headers,
      req: req.socket.remoteAddress,
    });
    next();
  };
}
