/* eslint-disable no-param-reassign */
/* eslint-disable no-unused-vars */
/**
 * Add the protocol that is used to params in all services.
 * @param {*} socket true - is a socket middleware
 *                      false - is a rest middleware
 */
export function ioProtocol(socket) {
  socket.feathers.protocol = "https";
}

export function restProtocol(app) {
  return (req, res, next) => {
    req.feathers.protocol = req.protocol;
    next();
  };
}
