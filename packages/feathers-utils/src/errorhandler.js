import { log } from "@lpgroup/utils";
import express from "@feathersjs/express";

const { error } = log("exception");

export function errorHandler() {
  return express.errorHandler({
    logger: (err, str, req) => {
      error(`Error in ${req.method} ${req.url}`);
    },
    // eslint-disable-next-line consistent-return
    html: (err, req, res, next) => {
      if (res.headersSent) {
        return next(err);
      }
      if (err.code === "500") {
        error(err);
      }
      res.status(err.code).send({ code: err.code, name: err.name, message: err.message });
    },
  });
}
