import { log } from "@lpgroup/utils";
import { getUrl } from "./utils/index.js";

const { info: dbgRequest } = log("req.ext");
const { info: debInternal } = log("req.srv");

// All external providers that can exist in params.provider.
const providers = ["rest", "socketio", "primus"];

export default () => {
  return (context) => {
    if (context.type === "before") {
      const url = getUrl(context, ["ready", "healthy"]);
      if (url) {
        if (providers.includes(context.params.provider)) dbgRequest(`${context.method} ${url}`);
        else debInternal(`${context.method} ${url}`);
      }
    }
  };
};
