/**
 * feathersjs supports both rest and websockets. Websockets
 * uses query-parameters instead of routing parameters. We decided
 * to only use query parameters everywhere. This code will MOVE
 * routing parameters to the query parameter.
 */

function convertToNumber(value) {
  const parsed = parseInt(value, 10);
  // eslint-disable-next-line no-restricted-globals
  if (!isNaN(parsed)) {
    return parsed;
  }
  return value;
}

function getParameterSearchOperator(value) {
  const searchOperator = {
    $or: true,
    $ne: true,
    $gt: true,
    $gte: true,
    $lt: true,
    $lte: true,
  };

  if (typeof value === "object") {
    const operator = Object.keys(value)[0];
    if (searchOperator[operator]) return operator;
  }

  return undefined;
}

// eslint-disable-next-line no-unused-vars
export default (options = {}) => {
  // TODO: Maybe we should user $populate, looks like feathersjs standard?
  const whiteList = ["populate", "unpopulate"];
  return async (context) => {
    const { params } = context;

    Object.keys(params.query || {}).forEach((query) => {
      // convert all ".at" unixtime to number
      const searchOperator = getParameterSearchOperator(params.query[query]);
      if (query.endsWith(".at")) {
        if (searchOperator) {
          params.query[query][searchOperator] = convertToNumber(
            params.query[query][searchOperator],
          );
        } else {
          params.query[query] = convertToNumber(params.query[query]);
        }
      }

      // trim all query params
      if (typeof params.query[query] === "string") {
        if (searchOperator) {
          params.query[query][searchOperator] = params.query[query][searchOperator].trim();
        } else {
          params.query[query] = params.query[query].trim();
        }
      }

      // Move whiteListed query parameters to params
      if (whiteList.includes(query)) {
        params[query] = params.query[query];
        delete params.query[query];
      }
    });

    Object.keys(params.route || {}).forEach((param) => {
      params.query[param] = params.route[param];
      delete params.route[param];
    });
    return context;
  };
};
