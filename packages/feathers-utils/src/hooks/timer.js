/**
 * A feathersjs hook that will record the time a request takes.
 *
 * Add this before and after to time a request.
 * Uses npm debug to write messages.
 */
import { log } from "@lpgroup/utils";
import { getUrl } from "./utils/index.js";

const { debug } = log("timer");

export default (threshold = 0) => {
  return async (context) => {
    const { params } = context;
    if (params?.performanceTimer) {
      const url = getUrl(context);
      const time = process.hrtime(params?.performanceTimer)[0];
      if (time >= threshold) {
        debug(`TIMER: ${context.method} ${url} - ${time} s`);
      }
      delete context.params.performanceTimer;
    } else {
      params.performanceTimer = process.hrtime();
    }
    return context;
  };
};
