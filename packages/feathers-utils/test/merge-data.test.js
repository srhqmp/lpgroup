import { cloneDeep } from "lodash-es";
import { mergeData } from "../src/merge-data.js";

describe("utils/merge-patch-data", () => {
  const sourceEmpty = {
    _id: "uuid-xxx",
    alias: "s-alias",
    content: { alias: "content-alias" },
    aliasActions: [],
    idActions: [],
    deepActions: [],
  };

  const sourceSimple = {
    _id: "uuid-xxx",
    alias: "s-alias",
    content: { alias: "content-alias" },
    aliasActions: [{ alias: "a-alias", type: "system", workerGroups: [{ aliasPack1: "one" }] }],
    idActions: [{ _id: "a-id", type: "system", workerGroups: [{ idPack1: "one" }] }],
    deepActions: [{ _id: "a-id", deep: [{ _id: "deep-1", idPack1: "one" }] }],
    simpleArray: ["one", "two", "three"],
  };

  const sourceDualRows = {
    _id: "uuid-xxx",
    alias: "s-alias",
    content: { alias: "content-alias" },
    aliasActions: [
      { alias: "a-alias", type: "system", workerGroups: [{ aliasPack1: "one" }] },
      { alias: "b-alias", type: "system", workerGroups: [{ aliasPack1: "two" }] },
    ],
    idActions: [
      { _id: "a-id", type: "system", workerGroups: [{ idPack1: "one" }] },
      { _id: "b-id", type: "system", workerGroups: [{ idPack1: "two" }] },
    ],
    deepActions: [
      {
        _id: "a-id",
        deep: [
          { _id: "deep-1", idPack1: "one" },
          { _id: "deep-2", idPack1: "two" },
        ],
      },
    ],
    simpleArray: ["one", "two", "three", "four", "five", "six"],
  };

  const sourceTripleRows = {
    _id: "uuid-xxx",
    alias: "s-alias",
    content: { alias: "content-alias" },
    aliasActions: [
      { alias: "a-alias", type: "system", workerGroups: [{ aliasPack1: "one" }] },
      { alias: "b-alias", type: "system", workerGroups: [{ aliasPack1: "two" }] },
      { alias: "c-alias", type: "system", workerGroups: [{ aliasPack1: "three" }] },
    ],
    idActions: [
      { _id: "a-id", type: "system", workerGroups: [{ idPack1: "one" }] },
      { _id: "b-id", type: "system", workerGroups: [{ idPack1: "two" }] },
      { _id: "c-id", type: "system", workerGroups: [{ idPack1: "three" }] },
    ],
    deepActions: [
      {
        _id: "a-id",
        deep: [
          { _id: "deep-1", idPack1: "one" },
          { _id: "deep-2", idPack1: "two" },
          { _id: "deep-3", idPack1: "three" },
        ],
      },
    ],
    simpleArray: ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"],
  };

  test("Merge two equal arrays", async () => {
    const source1 = cloneDeep(sourceSimple);
    const source2 = cloneDeep(sourceSimple);

    const sourceTarget = mergeData(source1, source2);
    expect(sourceTarget).toEqual(sourceSimple);
  });

  test("Add one item to empty array", async () => {
    const source1 = cloneDeep(sourceEmpty);
    const source2 = {
      aliasActions: [{ alias: "a-alias", type: "system", workerGroups: [{ aliasPack1: "one" }] }],
      idActions: [{ _id: "a-id", type: "system", workerGroups: [{ idPack1: "one" }] }],
      deepActions: [{ _id: "a-id", deep: [{ _id: "deep-1", idPack1: "one" }] }],
      simpleArray: ["one", "two", "three"],
    };
    const sourceTarget = mergeData(source1, source2);
    expect(sourceTarget).toEqual(sourceSimple);
  });

  test("Add one item to one item", async () => {
    const source1 = cloneDeep(sourceSimple);
    const source2 = {
      aliasActions: [{ alias: "b-alias", type: "system", workerGroups: [{ aliasPack1: "two" }] }],
      idActions: [{ _id: "b-id", type: "system", workerGroups: [{ idPack1: "two" }] }],
      deepActions: [{ _id: "a-id", deep: [{ _id: "deep-2", idPack1: "two" }] }],
      simpleArray: ["one", "two", "three", "four", "five", "six"],
    };
    const sourceTarget = mergeData(source1, source2);
    expect(sourceTarget).toEqual(sourceDualRows);
  });

  test("Add one item to two items", async () => {
    const source1 = cloneDeep(sourceDualRows);
    const source2 = {
      aliasActions: [{ alias: "c-alias", type: "system", workerGroups: [{ aliasPack1: "three" }] }],
      idActions: [{ _id: "c-id", type: "system", workerGroups: [{ idPack1: "three" }] }],
      deepActions: [{ _id: "a-id", deep: [{ _id: "deep-3", idPack1: "three" }] }],
      simpleArray: ["one", "two", "three", "four", "five", "six", "seven", "eight", "nine"],
    };
    const sourceTarget = mergeData(source1, source2);
    expect(sourceTarget).toEqual(sourceTripleRows);
  });

  test("Modify one of two items", async () => {
    const source1 = cloneDeep(sourceDualRows);
    const source2 = {
      aliasActions: [{ alias: "b-alias", type: "cow", workerGroups: [{ aliasPack1: "change" }] }],
      idActions: [{ _id: "b-id", type: "cow", workerGroups: [{ idPack1: "change" }] }],
      deepActions: [{ _id: "a-id", deep: [{ _id: "deep-2", idPack1: "change" }] }],
      simpleArray: ["oneCow", "twoCows", "threeCows"],
    };
    const sourceTarget = mergeData(source1, source2);
    const expected = cloneDeep(sourceDualRows);

    expected.aliasActions[1].type = "cow";
    expected.idActions[1].type = "cow";
    expected.aliasActions[1].workerGroups[0].aliasPack1 = "change";
    expected.idActions[1].workerGroups[0].idPack1 = "change";
    expected.deepActions[0].deep[1].idPack1 = "change";
    expected.simpleArray = ["oneCow", "twoCows", "threeCows"];
    expect(sourceTarget).toEqual(expected);
  });

  test("Remove one of two items", async () => {
    const source1 = cloneDeep(sourceDualRows);
    const source2 = {
      aliasActions: [{ alias: "b-alias" }],
      idActions: [{ _id: "b-id" }],
      deepActions: [{ _id: "a-id", deep: [{ _id: "deep-2" }] }],
      simpleArray: ["one", "two", "three"],
    };
    const sourceTarget = mergeData(source1, source2);
    expect(sourceTarget).toEqual(sourceSimple);
  });

  test("Replace simple array with empty array", async () => {
    const source1 = cloneDeep(sourceSimple);
    const source2 = {
      _id: "uuid-xxx",
      alias: "s-alias",
      content: { alias: "content-alias" },
      aliasActions: [],
      idActions: [],
      deepActions: [{ _id: "a-id", deep: [] }],
      simpleArray: [],
    };

    const sourceTarget = mergeData(source1, source2);
    const expected = cloneDeep(sourceSimple);
    expected.simpleArray = [];
    expect(sourceTarget).toEqual(expected);
  });

  test("Replace array when no _id or alias exist", async () => {
    const source1 = cloneDeep(sourceDualRows);
    const source2 = {
      aliasActions: [{ replace: "everything" }],
      idActions: [{ replace: "everything" }],
    };
    expect(() => {
      mergeData(source1, source2);
    }).toThrow("Both target and source need to have _id, alias or no _id, alias");
  });
});
