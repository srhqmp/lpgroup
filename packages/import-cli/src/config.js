import { log } from "@lpgroup/utils";
import Table from "table-layout";

const { info } = log("import-cli");

// Global config variable (array)
let gConf;

// Global config variable (Key/value object)
let goConf;

export function setConfig(parameters, config) {
  gConf = [
    { key: "version", name: "Version", value: parameters.version },
    { key: "environment", name: "Environment", value: parameters.environment },
    { key: "cwd", name: "CWD", value: parameters.cwd },
    { key: "extension", name: "Extension", value: parameters.extension },
    { key: "include", name: "Include", value: parameters.include || "NA" },
    { key: "exclude", name: "Exclude", value: parameters.exclude || "NA" },
    { key: "verbose", name: "Verbose", value: parameters.verbose },

    { key: "requiredKeys", name: "requiredKeys", value: config.requiredKeys || "NA" },
    { key: "ignoreKeyCompare", name: "ignoreKeyCompare", value: config.ignoreKeyCompare || "NA" },

    { key: "httpServer", name: "http.server", value: config.http.server || "NA" },
    { key: "httpUser", name: "http.user", value: config.http.user || "NA" },
    { key: "httpPassword", name: "http.password", value: "***" },
    { key: "httpReadyServer", name: "http.readyServer", value: config.http.readyServer || "NA" },
    { key: "httpMaxRPS", name: "http.maxRPS", value: config.http.maxRPS || "NA" },
    {
      key: "httpHeaders",
      name: "http.headers",
      value: JSON.stringify(config.http.headers) || "NA",
    },

    { key: "nats.uri", name: "nats.uri", value: config.nats.uri || "NA" },
    { key: "nats.queue", name: "nats.queue", value: config.nats.queue || "NA" },
    {
      key: "nats.messagePrefix",
      name: "nats.messagePrefix",
      value: config.nats.messagePrefix || "NA",
    },
    {
      key: "nats.version",
      name: "nats.version",
      value: config.nats.version || "NA",
    },
    { key: "rabbitmq.uri", name: "rabbitmq.uri", value: config.rabbitmq.uri || "NA" },
    { key: "rabbitmq.queue", name: "rabbitmq.queue", value: config.rabbitmq.queue || "NA" },
  ];

  goConf = gConf.reduce((acc, { key, value }) => {
    acc[key] = value;
    return acc;
  }, {});
}

export const configArr = () => gConf;
export const config = () => goConf;

export const isProduction = () => goConf.environment === "prod";

export const version = () => goConf.version;
export const environment = () => goConf.environment;
export const cwd = () => goConf.cwd;
export const extension = () => goConf.extension;
export const include = () => goConf.include;
export const exclude = () => goConf.exclude;
export const verbose = () => goConf.verbose;

export const requiredKeys = () => goConf.requiredKeys;
export const ignoreKeyCompare = () => goConf.ignoreKeyCompare;
export const httpServer = () => goConf.http.server;
export const httpUser = () => goConf.http.user;
export const httpPassword = () => goConf.httpPassword;
export const httpReadyServer = () => goConf.http.readyServer;
export const httpMaxRPS = () => goConf.http.maxRPS;
export const httpHeaders = () => JSON.stringify(goConf.http.headers);
export const natsUri = () => goConf.nats.uri;
export const natsQueue = () => goConf.nats.queue;
export const natsMessagePrefix = () => goConf.nats.messagePrefix;
export const natsVersion = () => goConf.nats.version;
export const rabbitmqUri = () => goConf.rabbitmq.uri;
export const rabbitmqQueue = () => goConf.rabbitmq.queue;

export function printConfig() {
  info(`import-cli (${config().version})`);
  const table = new Table(
    configArr().map((v) => ({ name: v.name, value: v.value })),
    { maxWidth: 100 },
  );
  info(table.toString());
}
