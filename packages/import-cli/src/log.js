/* eslint-disable no-console */

export const description = (e) => console.log("\x1b[33m%s\x1b[0m", e);
export const she = (e) => console.log("\x1b[32m", e, "\x1b[0m");
export const info = (e) => console.log("\x1b[36m", e, "\x1b[0m");
export const comment = (e) => console.log("\x1b[30m", "\x1b[47m", e, "\x1b[0m");
