#!/usr/bin/env node --experimental-modules --es-module-specifier-resolution=node

// eslint-disable-next-line node/no-unpublished-bin
import debugSettings from "debug";
import { log } from "@lpgroup/utils";
import { executeDirectory, generateWhiteList } from "@lpgroup/feathers-utils";
import { getCmdLineArgs, getConfig } from "./src/parameters.js";
import { setupPlugins, closePlugins } from "./src/plugins.js";
import { setConfig, printConfig } from "./src/config.js";

const { info, error } = log("import-cli");

async function main() {
  const parameters = getCmdLineArgs();
  const config = await getConfig(parameters.environment);
  setConfig(parameters, config);
  console.time("Time");

  if (parameters.verbose) {
    // Debug output to stdout.
    if (parameters.verbose) {
      let settings = "lpgroup:*:*";
      if (debugSettings.load()) settings = `${debugSettings.load()},${settings}`;

      debugSettings.enable(settings);
      debugSettings.save(settings);
    }
  }
  printConfig();

  // The script can handle one server of each type
  setupPlugins(config).then(() => {
    const options = {
      environment: parameters.environment,
      include: generateWhiteList(parameters.extension, parameters.include, parameters.exclude),
      verbose: parameters.verbose,
    };
    executeDirectory(parameters.cwd, options).then(() => {
      // eslint-disable-next-line no-console
      info("Done");
      console.timeEnd("Time");
      closePlugins();
    });
  });
}

main().catch((err) => {
  error(err.message);
  error(err);
});
