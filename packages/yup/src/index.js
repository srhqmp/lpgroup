import * as yup from "yup";

/**
 * defaultNull - field can be null and defaults to null
 *
 * exammple: password: cy.string().defaultNull(),
 */
// eslint-disable-next-line func-names
yup.addMethod(yup.Schema, "defaultNull", function () {
  return this.nullable().default(null);
});

export { yup };
export { mixed, lazy, boolean, number } from "yup";
export * from "./utils.js";
export * from "./general.js";
export * from "./standard.js";
export * from "./hooks.js";
export * from "./YupError.js";
