import * as cy from "../src/index.js";

function validate(data, schema) {
  const valid = cy.validate(data, schema);
  // eslint-disable-next-line jest/valid-expect
  return expect(valid);
}

describe("Utils", () => {
  test("buildValidationSchema.json", async () => {
    const schema = cy.buildValidationSchema({ alias: cy.alias() }, { dbAlias: cy.alias() });

    validate({ alias: "this-is-a-test" }, schema.request).resolves.toStrictEqual({
      alias: "this-is-a-test",
    });

    const resolvedSchema = schema.all.resolve({ value: { alias: "this-is-a-test" } });
    expect(Object.keys(resolvedSchema.fields).sort()).toStrictEqual(
      ["alias", "dbAlias", "myLock"].sort(),
    );
  });

  test("buildValidationSchema.lazy", async () => {
    const schema = cy.buildValidationSchema(
      () => ({ alias: cy.alias() }),
      () => ({ dbAlias: cy.alias() }),
    );

    validate({ alias: "this-is-a-test" }, schema.request).resolves.toStrictEqual({
      alias: "this-is-a-test",
    });

    const resolvedSchema = schema.all.resolve({ value: { alias: "this-is-a-test" } });
    expect(Object.keys(resolvedSchema.fields)).toStrictEqual(["dbAlias", "myLock", "alias"]);
  });

  test("buildValidationSchema.override", async () => {
    const schema = cy.buildValidationSchema(
      () => ({ alias1: cy.alias() }),
      () => ({ dbAlias1: cy.alias() }),
    );

    const overrideSchema = cy.buildValidationSchema(
      () => ({ alias2: cy.alias() }),
      () => ({ dbAlias2: cy.alias() }),
    );

    const mergedSchema = schema.override(overrideSchema);

    validate(
      { alias1: "this-is-a-test1", alias2: "this-is-a-test2" },
      mergedSchema.request,
    ).resolves.toStrictEqual({
      alias1: "this-is-a-test1",
      alias2: "this-is-a-test2",
    });

    const resolvedRequestSchema = mergedSchema.request.resolve({
      value: { alias1: "this-is-a-test" },
    });
    expect(Object.keys(resolvedRequestSchema.fields).sort()).toStrictEqual(
      ["alias1", "alias2"].sort(),
    );

    const resolvedDbSchema = mergedSchema.db.resolve({ value: { alias1: "this-is-a-test" } });
    expect(Object.keys(resolvedDbSchema.fields).sort()).toStrictEqual(
      ["dbAlias1", "dbAlias2", "myLock"].sort(),
    );

    const resolvedAllSchema = mergedSchema.all.resolve({ value: { alias1: "this-is-a-test" } });
    expect(Object.keys(resolvedAllSchema.fields).sort()).toStrictEqual(
      ["alias1", "alias2", "dbAlias1", "dbAlias2", "myLock"].sort(),
    );
  });

  test("getXKeys", async () => {
    const schema = cy.buildValidationSchema(
      () => ({ alias1: cy.alias() }),
      () => ({ dbAlias1: cy.alias() }),
    );

    const overrideSchema = cy.buildValidationSchema(
      () => ({ alias2: cy.alias() }),
      () => ({ dbAlias2: cy.alias() }),
    );

    const mergedSchema = schema.override(overrideSchema);

    expect(schema.getRequestKeys().sort()).toStrictEqual(["alias1"].sort());
    expect(overrideSchema.getRequestKeys().sort()).toStrictEqual(["alias2"].sort());
    expect(mergedSchema.getRequestKeys().sort()).toStrictEqual(["alias1", "alias2"].sort());

    expect(schema.getDbKeys().sort()).toStrictEqual(["dbAlias1", "myLock"].sort());
    expect(overrideSchema.getDbKeys().sort()).toStrictEqual(["dbAlias2", "myLock"].sort());
    expect(mergedSchema.getDbKeys().sort()).toStrictEqual(
      ["dbAlias1", "dbAlias2", "myLock"].sort(),
    );

    expect(schema.getAllKeys().sort()).toStrictEqual(["alias1", "dbAlias1", "myLock"].sort());
    expect(overrideSchema.getAllKeys().sort()).toStrictEqual(
      ["alias2", "dbAlias2", "myLock"].sort(),
    );
    expect(mergedSchema.getAllKeys().sort()).toStrictEqual(
      ["alias1", "alias2", "dbAlias1", "dbAlias2", "myLock"].sort(),
    );
  });
});
