import { buildValidationSchema, validReq, validDB, string } from "../src/index.js";

function buildContext(data, requestSchema, databaseSchema) {
  return {
    data,
    method: "create",
    service: {
      options: {
        schema: buildValidationSchema(requestSchema, databaseSchema),
      },
    },
  };
}

describe("Hooks", () => {
  describe("validReq", () => {
    const schema = { key: string().required() };
    const validReqHook = validReq();

    test("valid", async () => {
      const context = buildContext({ key: "valid " }, schema, schema);
      await expect(validReqHook(context)).resolves.toEqual(
        expect.objectContaining({
          data: { key: "valid" },
        }),
      );
    });

    test("invalid", async () => {
      const context = buildContext({}, schema, schema);
      await expect(validReqHook(context)).rejects.toThrow("key is a required field");
    });
  });

  describe("validDB", () => {
    const schema = { key: string().required() };
    const validDBHook = validDB();

    test("valid", async () => {
      const context = buildContext({ key: "valid " }, schema, schema);
      await expect(validDBHook(context)).resolves.toEqual(
        expect.objectContaining({
          data: { key: "valid" },
        }),
      );
    });

    test("invalid", async () => {
      const context = buildContext({}, schema, schema);
      await expect(validDBHook(context)).rejects.toThrow("key is a required field");
    });
  });
});
