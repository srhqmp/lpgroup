// https://docs.feathersjs.com/cookbook/authentication/apikey.html

import { digestPassword } from "../utils.js";

export default () => {
  return async (context) => {
    const { params } = context;
    try {
      const plainToken =
        params?.headers && (params?.headers["api-key"] || params?.headers["x-api-key"]);

      if (plainToken && params.provider && !params.authentication) {
        const enigma = context.app.settings.authentication.apiKeySecret;
        const apiKey = digestPassword(plainToken, enigma);
        context.params = {
          ...params,
          authentication: {
            strategy: "apiKey",
            token: apiKey,
          },
        };
      }
    } catch {
      /* */
    }
    return context;
  };
};
