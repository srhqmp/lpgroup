/* eslint-disable no-param-reassign */
import { buildItemHook } from "@lpgroup/feathers-utils";
import { digestPassword } from "../utils.js";

// eslint-disable-next-line no-unused-vars
export default (key) => {
  return buildItemHook((context, item) => {
    const enigma = context.app.settings.authentication.apiKeySecret;
    if (item[key]) item[key] = digestPassword(item[key], enigma);
  });
};
