import { internalParams } from "@lpgroup/feathers-utils";
import { hooks } from "@feathersjs/authentication";
import { combine } from "feathers-hooks-common";
// eslint-disable-next-line import/no-named-default
import { default as checkPermissions } from "./check-permissions.js";
import allowApiKey from "./allow-apiKey.js";

const { authenticate } = hooks;

async function setUser(context, defaultUserEmail) {
  const user = await context.app
    .service("users")
    .find(internalParams({ ...context.params, superUser: true }, { email: defaultUserEmail }));
  // eslint-disable-next-line prefer-destructuring
  context.params.user = user.data[0];
  return context;
}

export default (extraPrivilege, defaultUserEmail = null) => {
  return async (context) => {
    // Only validate auth token if it exist. Otherwise let checkPermission return
    // error message, if no public privileges exist.
    if (
      context.params.authentication ||
      (context.params.headers &&
        (context.params.headers["x-api-key"] || context.params.headers["api-key"]))
    ) {
      return combine(
        allowApiKey(),
        authenticate("jwt", "apiKey"),
        checkPermissions(extraPrivilege),
      ).call(this, context);
    }

    if (defaultUserEmail) await setUser(context, defaultUserEmail);
    return checkPermissions(extraPrivilege)(context);
  };
};
