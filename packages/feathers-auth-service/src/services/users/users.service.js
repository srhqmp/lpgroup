import { Users } from "./users.class.js";
import hooks from "./users.hooks.js";
import schema from "./users.yup.js";

export default (app, optionsOverride = {}) => {
  const options = {
    id: "_id",
    paginate: app.get("paginate"),
    schema: optionsOverride?.schema ? schema.override(optionsOverride.schema) : schema,
  };

  if (optionsOverride?.Users) {
    app.use("/users", new optionsOverride.Users(options, app));
  } else {
    app.use("/users", new Users(options, app));
  }
  const service = app.service("users");
  service.hooks(hooks);
};

export { Users };
