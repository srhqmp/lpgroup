import { YupError } from "@lpgroup/yup";
import errors from "@feathersjs/errors";
import { preValidate } from "../users.yup.js";

export default (keys) => {
  return async (context) => {
    try {
      await preValidate.pick(keys).validate(context.data);
    } catch (err) {
      throw new errors.GeneralError(new YupError(err));
    }
    return context;
  };
};
