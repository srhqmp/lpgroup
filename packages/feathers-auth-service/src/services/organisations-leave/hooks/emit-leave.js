// eslint-disable-next-line no-unused-vars
export default (options = {}) => {
  return async (context) => {
    context.app.emit("leave", context);
    return context;
  };
};
