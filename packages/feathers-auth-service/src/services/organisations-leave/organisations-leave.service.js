import { OrganisationsLeave } from "./organisations-leave.class.js";
import hooks from "./organisations-leave.hooks.js";

export default (app) => {
  app.use("/organisations/:organisationAlias/leave", new OrganisationsLeave());
  const service = app.service("organisations/:organisationAlias/leave");
  service.hooks(hooks);
};
