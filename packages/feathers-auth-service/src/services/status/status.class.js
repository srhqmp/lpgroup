export class Status {
  constructor(options, app) {
    this.options = options || {};
    this.app = app;
    this.started = Date.now();
  }

  // eslint-disable-next-line no-unused-vars
  async find(params) {
    const time = Date.now();
    const uptimeSeconds = Math.abs(this.started - time) / 1000;
    return {
      application: this.app.get("application"),
      version: this.app.get("version"),
      env: this.app.get("env"),
      environment: this.app.get("environment"),
      started: this.started,
      time,
      uptimeSeconds,
    };
  }
}
