import { UsersUpgrade } from "./users-upgrade.class.js";
import hooks from "./users-upgrade.hooks.js";
import schema from "./users-upgrade.yup.js";

export default (app) => {
  const options = {
    id: "_id",
    schema,
  };
  app.use("/users/:userId/upgrade", new UsersUpgrade(options, app));
  const service = app.service("users/:userId/upgrade");
  service.hooks(hooks);
};

export { UsersUpgrade };
