import { Service } from "feathers-mongodb";
import { onPluginReady } from "@lpgroup/feathers-plugins";
import { internalParams } from "@lpgroup/feathers-utils";

export class UsersApiKeys extends Service {
  constructor(options, app) {
    super(options);
    this.app = app;

    onPluginReady("mongodb").then(({ database }) => {
      this.Model = database.collection("users-api-keys");
      database.createIndexThrowError("users-api-keys", { apiKey: 1 }, { unique: true });
    });
  }

  async create(data, params) {
    const userId = params.query?.userId;
    // the `get` below prevents 'create' when user does not exist, throws 404
    const user = await this.app.service("users").get(userId, internalParams({ superUser: true }));
    const name = data.name || `${user.firstName} ${user.lastName}`;
    const newData = { ...data, name };
    return super.create(newData, params);
  }
}
