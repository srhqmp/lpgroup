// eslint-disable-next-line no-unused-vars
export default (options = {}) => {
  return async (context) => {
    const { data, params } = context;
    data.userId = params.query.userId;
    return context;
  };
};
