import { UsersApiKeys } from "./users-api-keys.class.js";
import hooks from "./users-api-keys.hooks.js";
import schema from "./users-api-keys.yup.js";

export default (app) => {
  const options = {
    id: "_id",
    paginate: app.get("paginate"),
    schema,
  };
  app.use("/users/:userId/api-keys", new UsersApiKeys(options, app));
  const service = app.service("users/:userId/api-keys");
  service.hooks(hooks);
};
