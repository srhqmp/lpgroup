import errors from "@feathersjs/errors";
import { oAuthWithFacebook, getFacebookOauthUrl } from "./providers/facebook-provider.js";
import { getGithubOauthUrl, oAuthWithGithub } from "./providers/github-provider.js";
import { oAuthWithGoogle, getGoogleOauthUrl } from "./providers/google-provider.js";

export class OAuth {
  constructor(options, app) {
    this.options = options || {};
    this.app = app;
  }

  async find(params) {
    const googleLink = getGoogleOauthUrl(this.app, params);
    const facebookLink = getFacebookOauthUrl(this.app, params);
    const githubLink = getGithubOauthUrl(this.app, params);
    return { googleLink, facebookLink, githubLink };
  }

  // id is === "callback"
  async get(id, params) {
    const provider = params.query?.provider;
    const oauthConfig = this.app.get("authentication").oauth;
    const responseRedirect = (obj = {}) => {
      const qs = new URLSearchParams(obj);
      return Promise.resolve({ url: `${oauthConfig.redirect}?${qs.toString()}` });
    };
    if (provider === "google") return oAuthWithGoogle(this.app, params, responseRedirect);
    if (provider === "facebook") return oAuthWithFacebook(this.app, params, responseRedirect);
    if (provider === "github") return oAuthWithGithub(this.app, params, responseRedirect);
    return new errors.GeneralError("Oauth method not recognized");
  }
}
