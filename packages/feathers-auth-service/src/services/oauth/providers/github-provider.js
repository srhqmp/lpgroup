export function getGithubOauthUrl(app, params) {
  const githubConfig = app.get("authentication").oauth.github;
  const { key, redirectRelativePath } = githubConfig;
  const domain = `${params.protocol}://${params.headers.host}`;
  const rootUrl = "https://github.com/login/oauth/authorize";
  const options = {
    redirect_uri: `${domain}/${redirectRelativePath}`,
    client_id: key,
    prompt: "consent",
    scope: ["user:email"].join(","),
  };
  const qs = new URLSearchParams(options);
  return `${rootUrl}?${qs.toString()}`;
}

export async function oAuthWithGithub(app, params, responseRedirect) {
  try {
    const auth = await app.service("authentication").create({ strategy: "github" }, params);
    return responseRedirect({ isError: false, accessToken: auth.accessToken });
  } catch (err) {
    return responseRedirect({ isError: true, message: err.message });
  }
}
