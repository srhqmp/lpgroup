export function getGoogleOauthUrl(app, params) {
  const googleConfig = app.get("authentication").oauth.google;
  const { key, redirectRelativePath } = googleConfig;
  const domain = `${params.protocol}://${params.headers.host}`;
  const rootUrl = "https://accounts.google.com/o/oauth2/v2/auth";
  const options = {
    redirect_uri: `${domain}/${redirectRelativePath}`,
    client_id: key,
    access_type: "offline",
    response_type: "code",
    prompt: "consent",
    scope: [
      "https://www.googleapis.com/auth/userinfo.profile",
      "https://www.googleapis.com/auth/userinfo.email",
    ].join(" "),
  };
  const qs = new URLSearchParams(options);
  return `${rootUrl}?${qs.toString()}`;
}

export async function oAuthWithGoogle(app, params, responseRedirect) {
  try {
    const auth = await app.service("authentication").create({ strategy: "google" }, params);
    return responseRedirect({ isError: false, accessToken: auth.accessToken });
  } catch (err) {
    return responseRedirect({ isError: true, message: err.message });
  }
}
