import * as yup from "@lpgroup/yup";

const requestSchema = {
  userId: yup.uuid().required(),
  // TODO: WARNING: Security error, anyone having access to grants. Can give access to anything.
  privilegie: yup.labelText().default("standard_organisation"),
};

export default yup.buildValidationSchema(requestSchema);
