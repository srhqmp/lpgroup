import { ServiceCache } from "@lpgroup/feathers-utils";
import { onPluginReady } from "@lpgroup/feathers-plugins";

export class Privileges extends ServiceCache {
  constructor(options) {
    super(options);
    onPluginReady("mongodb").then(({ database }) => {
      this.Model = database.collection("privileges");
      database.createIndexThrowError("privileges", { alias: 1 }, { unique: true });
    });
  }
}
