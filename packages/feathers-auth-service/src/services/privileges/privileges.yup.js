import * as cy from "@lpgroup/yup";

const requestSchema = {
  _id: cy.id(),
  alias: cy.alias().required(),
  name: cy.labelText().required(),
  permissions: cy.lazyObject({
    query: cy.arrayObject({
      key: cy.mediumText().required(),
      param: cy.mediumText().required(),
    }),
    methods: cy.array(cy.mediumText()),
  }),
};

const dbSchema = {
  added: cy.changed(),
  changed: cy.changed(),
  owner: cy.userOwner(),
};

export default cy.buildValidationSchema(requestSchema, dbSchema);
