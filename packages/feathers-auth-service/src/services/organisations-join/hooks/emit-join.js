// eslint-disable-next-line no-unused-vars
export default (options = {}) => {
  return async (context) => {
    context.app.emit("join", context);
    return context;
  };
};
