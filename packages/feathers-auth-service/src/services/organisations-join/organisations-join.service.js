import { OrganisationsJoin } from "./organisations-join.class.js";
import hooks from "./organisations-join.hooks.js";

export default (app) => {
  const options = {
    id: "_id",
  };

  app.use("/organisations/:organisationAlias/join", new OrganisationsJoin(options, app));
  const service = app.service("organisations/:organisationAlias/join");
  service.hooks(hooks);
};
