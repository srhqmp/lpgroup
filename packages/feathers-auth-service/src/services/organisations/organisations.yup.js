import * as yup from "@lpgroup/yup";

export const requestSchema = {
  _id: yup.id(),
  alias: yup.alias().required(),
  name: yup.labelText().required(),
  organisationNumber: yup.organisationNumber(),
  email: yup.email().defaultNull(),
  phone: yup.phone().defaultNull(),
  website: yup.url().defaultNull(),
  contact: yup.emailContact(),
  postAddress: yup.addressObject(),
};

const dbSchema = {
  added: yup.changed(),
  changed: yup.changed(),
  owner: yup.owner(),
};

export default yup.buildValidationSchema(requestSchema, dbSchema);
