import { loadData, patchData } from "@lpgroup/feathers-mongodb-hooks/hooks";
import { url } from "@lpgroup/feathers-utils/hooks";
import { validDB, validReq } from "@lpgroup/yup";
import search from "feathers-mongodb-fuzzy-search";

import auth from "../../hooks/auth.js";
import changed from "../../hooks/changed.js";
import grantPrivileges from "./hooks/grant-privileges.js";
import setOwner from "./hooks/set-owner-organisation.js";

export default {
  before: {
    all: [],
    find: [
      auth("public"),
      search({
        // regex search on given fields
        fields: ["alias", "name", "organisationNumber", "email", "phone", "website"],
      }),
    ],
    get: [auth("public")],
    create: [auth(), validReq(), changed(), setOwner(), validDB()],
    update: [auth(), validReq(), loadData(), changed(), validDB()],
    patch: [auth(), validReq(), patchData(), changed(), validDB()],
    remove: [auth()],
  },

  after: {
    all: [url({ key: "alias" })],
    find: [],
    get: [],
    create: [grantPrivileges()],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
