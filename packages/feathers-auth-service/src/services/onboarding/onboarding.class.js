export class Onboarding {
  async find(params) {
    const host = `${params.protocol}://${params.headers.host}`;

    return {
      "register-user_url": `${host}/onboarding/register-user`,
    };
  }
}
