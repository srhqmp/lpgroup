import { disallow, discard, iff, isProvider } from "feathers-hooks-common";
import { validReq } from "@lpgroup/yup";

export default {
  before: {
    all: [],
    find: [disallow()],
    get: [disallow()],
    create: [validReq()],
    update: [disallow()],
    patch: [disallow()],
    remove: [disallow()],
  },

  after: {
    all: [
      iff(
        isProvider("external"),
        discard(
          "privileges",
          "owner",
          "added",
          "changed",
          "password",
          "signInCode",
          "signInEnabled",
        ),
      ),
    ],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
