import { OnboardingRegisterUser } from "./register-user.class.js";
import hooks from "./register-user.hooks.js";
import schema from "./register-user.yup.js";

export default (app) => {
  const options = { schema };
  app.use("/onboarding/register-user", new OnboardingRegisterUser(options, app));
  const service = app.service("/onboarding/register-user");
  service.hooks(hooks);
};
