import * as yup from "@lpgroup/yup";

const requestSchema = {
  _id: yup.id(),
  email: yup.email().required(),
  password: yup.password().required(),
  firstName: yup.labelText().required(),
  lastName: yup.labelText().required(),
  phone: yup.phone().defaultNull(),
};

const dbSchema = {};

export default yup.buildValidationSchema(requestSchema, dbSchema);
