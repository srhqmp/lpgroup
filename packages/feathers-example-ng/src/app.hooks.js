import { startSession, endSession, errorSession } from "@lpgroup/feathers-plugins/mongodb";
import { requestLog, errorHandler, setQuery } from "@lpgroup/feathers-utils/hooks";
import { discard, iff, isProvider } from "feathers-hooks-common";

export default {
  before: {
    all: [requestLog(), setQuery()],
    find: [],
    get: [],
    create: [startSession()],
    update: [startSession()],
    patch: [startSession()],
    remove: [startSession()],
  },

  after: {
    all: [iff(isProvider("external"), discard("organisationAlias", "myLock", "password"))],
    find: [],
    get: [],
    create: [endSession()],
    update: [endSession()],
    patch: [endSession()],
    remove: [endSession()],
  },

  error: {
    all: [errorHandler()],
    find: [],
    get: [],
    create: [errorSession()],
    update: [errorSession()],
    patch: [errorSession()],
    remove: [errorSession()],
  },
};
