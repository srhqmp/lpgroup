import { auth } from "@lpgroup/feathers-auth-service/hooks";
import { url } from "@lpgroup/feathers-utils/hooks";
import { patchData, loadData } from "@lpgroup/feathers-mongodb-hooks/hooks";
import { validReq, validDB } from "@lpgroup/yup";
import { preventChanges } from "feathers-hooks-common";

export default {
  before: {
    all: [auth("public")],
    find: [],
    get: [],
    create: [validReq(), validDB()],
    update: [validReq(), loadData(), validDB()],
    patch: [preventChanges(true, "type"), validReq(), patchData(), validDB()],
    remove: [],
  },

  after: {
    all: [url({ key: "_id" })],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
