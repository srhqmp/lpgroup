import { organisations } from "@lpgroup/feathers-auth-service";
import schema from "./organisations.yup.js";

export default (app) => {
  return organisations(app, { schema });
};
