/* eslint-disable no-unused-vars */
import errors from "@feathersjs/errors";

export class BadGateWay {
  constructor(options, app) {
    this.options = options || {};
    this.app = app;
    this.started = Date.now();
  }

  async find(params) {
    throw new errors.BadGateway();
  }

  async get(id) {
    if (id === "500") throw new errors.GeneralError();
    if (id === "501") throw new errors.NotImplemented();
    if (id === "502") throw new errors.BadGateway();
    if (id === "503") throw new errors.Unavailable();
    throw new errors.BadGateway();
  }

  async create(data) {
    throw new errors.BadGateway();
  }

  async patch(data) {
    throw new errors.BadGateway();
  }

  async update(data) {
    throw new errors.BadGateway();
  }

  async remove(data) {
    throw new errors.BadGateway();
  }
}
