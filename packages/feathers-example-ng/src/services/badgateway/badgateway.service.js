import { BadGateWay } from "./badgateway.class.js";
import hooks from "./badgateway.hooks.js";

export default (app) => {
  const options = {
    paginate: app.get("paginate"),
  };
  app.use("/badgateway", new BadGateWay(options, app));
  const service = app.service("badgateway");
  service.hooks(hooks);
};
