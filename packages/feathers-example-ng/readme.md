# feathers-example-ng

REST api for feathers-example

## Install and run in development mode

```sh
// Install node defined in .nvmrc
nvm use

// Install all npm packages
yarn

// Start service
yarn workspace @lpgroup/feathers-example-ng dev
```

## Import testdata

```sh
nvm use
yarn workspace @lpgroup/feathers-example-ng lpimport
```

## Endpoints

- carcosa.se/api/v1/onboarding/standard-user
- carcosa.se/api/v1/autenticate
- carcosa.se/api/v1/users
- carcosa.se/api/v1/organisations

## Access api with curl

```sh
HOST=http://localhost:8290
TOKEN=$(\
curl ${HOST}/authentication \
  -s -X POST -H 'Content-Type: application/json' \
  --data '{"strategy": "local", "email":"onboarding@carcosa.se","password":"The-Yellow-King@123"}' \
  | jq -r '.accessToken' \
)
echo $TOKEN

# Make an authenticated request with a token
curl ${HOST}/organisations/carcosa/grants \
  -s -H "Authorization: ${TOKEN}" | jq

# Make an authenticated request with an api-key
export API_KEY=dim-carcosa
curl ${HOST}/organisations/carcosa/grants \
  -s -H "api-key: ${API_KEY}" | jq

```
