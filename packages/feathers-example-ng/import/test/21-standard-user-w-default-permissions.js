/*
To peform all Standar User permission granted by default:

      "users": userGUP,
      "users/:userId/permissions": userGFUP,
      "users/:userId/organisations": userF,
      "organisations": userC,
      "organisations/:organisationAlias/join": allC,
      "organisations/:organisationAlias/users": ["find"],
*/

// dependencies: 20-organizations.js

import { axios, she, comment, info, description } from "@lpgroup/import-cli";

export const carcosaOrganization = {
  alias: "carcosa",
  name: "Carcosa R&D",
  title: "Carcosa R&D",
};

export const standardUser1 = {
  _id: "d373c10c-5f62-4577-881b-f7a5ee450b6d",
  email: "user1@carcosa.se",
  password: "The-Green-Lantern@123",
  firstName: "First user1",
  lastName: "Last user1",
  phone: "111-8888888",
};

export const standardUser2 = {
  _id: "c5ae7467-bcce-4f23-b55b-4a71719cd0d2",
  email: "user2@carcosa.se",
  password: "The-Green-Lantern@123",
  firstName: "First user1",
  lastName: "Last user1",
  phone: "111-8888888",
};

export default async () => {
  return axios().then(async (ax) => {
    description("Cleanup");
    await ax.login().then(async (o) => {
      await o.remove(`/users/${standardUser1._id}`, { ignoreError: true });
      await o.remove(`/users/${standardUser2._id}`, { ignoreError: true });
      await o.remove("/organisations/broker2", { ignoreError: true });
    });

    description("Standard User performs regular actions");

    she("can create own credentials");
    await ax.post("/onboarding/register-user", standardUser1, {
      expected: {
        _id: standardUser1._id,
        email: standardUser1.email,
        profileImageUrl: null,
        firstName: standardUser1.firstName,
        lastName: standardUser1.lastName,
        active: true,
        type: "user",
        onlineStatus: "ONLINE",
        isOnline: false,
        oAuthStrategy: null,
        userAgent: null,
        ipNumber: null,
        verified: false,
        phone: standardUser1.phone,
        url: `http://localhost:8290/users/${standardUser1._id}`,
      },
    });

    await ax.post("/onboarding/register-user", standardUser2, {
      expected: {
        _id: standardUser2._id,
        email: standardUser2.email,
        firstName: standardUser2.firstName,
        lastName: standardUser2.lastName,
        active: true,
        type: "user",
        onlineStatus: "ONLINE",
        isOnline: false,
        profileImageUrl: null,
        oAuthStrategy: null,
        userAgent: null,
        ipNumber: null,
        verified: false,
        phone: standardUser2.phone,
        url: `http://localhost:8290/users/${standardUser2._id}`,
      },
    });

    she("can login using local credentials and get accessToken");
    await ax.post(
      `/authentication`,
      {
        strategy: "local",
        email: standardUser1.email,
        password: standardUser1.password,
      },
      {
        expected: {
          accessToken: "string",
          user: {
            _id: standardUser1._id,
            type: "user",
            firstName: standardUser1.firstName,
            lastName: standardUser1.lastName,
            phone: standardUser1.phone,
            email: standardUser1.email,
            onlineStatus: "ONLINE",
            isOnline: false,
            userAgent: "axios/1.6.8",
            ipNumber: "::ffff:127.0.0.1",
            verified: false,
            profileImageUrl: null,
            privileges: [
              {
                params: {
                  organisationAlias: null,
                  userId: "d373c10c-5f62-4577-881b-f7a5ee450b6d",
                },
                privilegesAlias: "standard_user",
                _id: "c43a7b5f-f5f9-4fd3-a08c-7283ba62978e",
              },
            ],
          },
        },
        ignoreKeyCompare: ["accessToken", "privileges", "ipNumber"],
      },
    );

    await ax.login(standardUser1.email, standardUser1.password).then(async (o) => {
      she("can perform `get` to own id");
      const expected = {
        _id: standardUser1._id,
        email: standardUser1.email,
        active: true,
        type: "user",
        onlineStatus: "ONLINE",
        isOnline: false,
        profileImageUrl: null,
        oAuthStrategy: null,
        userAgent: "axios/1.6.8",
        ipNumber: "::ffff:127.0.0.1",
        verified: false,
        phone: standardUser1.phone,
        lastName: standardUser1.lastName,
        firstName: standardUser1.firstName,
        owner: { user: { _id: standardUser1._id } },
        changed: { by: "superuser", at: 1634002877223 },
        added: { by: "superuser", at: 1634002877223 },
        url: `http://localhost:8290/users/${standardUser1._id}`,
      };

      await o.get(`/users/${standardUser1._id}`, {
        expected,
        ignoreKeyCompare: ["changed", "added", "ipNumber"],
      });

      info("can not perform `get` to other users(onboarding) id");
      await o.get(`/users/${standardUser2._id}`, {
        expected: {
          name: "NotFound",
          message: `No record found for id '${standardUser2._id}'`,
          code: 404,
          className: "not-found",
          errors: {},
        },
        ignoreKeyCompare: ["changed", "added", "ipNumber"],
      });

      she("can perform `patch` to own id");
      await o.patch(
        `/users/${standardUser1._id}`,
        {
          phone: "234523-123",
        },
        {
          expected: { ...expected, phone: "234523-123" },
        },
      );

      she("can join existing organisations");
      // TODO: We shouldn't be able to join an organisation we don't have access to.
      await o.post(`/organisations/${carcosaOrganization.alias}/join`, null, {
        expected: {},
      });

      she("can perform `put` to own id");
      // await o.put(
      //   `/users/${standardUser1._id}`,
      //   {
      //     _id: standardUser1._id,
      //     email: standardUser1.email,
      //     firstName: standardUser1.firstName,
      //     lastName: standardUser1.lastName,
      //     phone: standardUser1.phone,
      //     password: standardUser1.password,
      //   },
      //   {
      //     expected: {
      //       _id: standardUser1._id,
      //       owner: {
      //         user: {
      //           _id: standardUser1._id,
      //         },
      //       },
      //       changed: {
      //         by: standardUser1._id,
      //         at: 1634024376887,
      //       },
      //       added: {
      //         by: "superuser",
      //         at: 1634024372662,
      //       },
      //       active: true,
      //       type: "device",
      //       phone: standardUser1.phone,
      //       lastName: standardUser1.lastName,
      //       firstName: standardUser1.firstName,
      //       email: standardUser1.email,
      //       url: `http://localhost:8290/users/${standardUser1._id}`,
      //     },
      //   }
      // );

      she("can create organisation");
      const orgData = {
        alias: "broker2",
        name: "Broker2 LTD.",
        title: "Broker2 LTD.",
        organisationNumber: "202100-5489",
      };
      const orgExpected = {
        _id: "8eb7af13-b3b9-4378-8feb-935271775ce5",
        images: [],
        theme: {
          color: {
            secondary: "#cccccc",
            primary: "#666666",
          },
        },
        location: {
          longitude: null,
          latitude: null,
        },
        visitAddress: {
          country: null,
          state: null,
          city: null,
          zipcode: null,
          coAddress: null,
          address: null,
        },
        description: {
          text: null,
          short: null,
        },
        companyName: null,
        title: "Broker2 LTD.",
        postAddress: {
          country: null,
          state: null,
          city: null,
          zipcode: null,
          coAddress: null,
          address: null,
        },
        contact: {
          phone: null,
          email: null,
          firstName: null,
          lastName: null,
        },
        website: null,
        phone: null,
        email: null,
        organisationNumber: "2021005489",
        name: "Broker2 LTD.",
        alias: "broker2",
        changed: {
          by: "d373c10c-5f62-4577-881b-f7a5ee450b6d",
          at: 1634039699523,
        },
        added: {
          by: "d373c10c-5f62-4577-881b-f7a5ee450b6d",
          at: 1634039699523,
        },
        owner: {
          user: {
            _id: "d373c10c-5f62-4577-881b-f7a5ee450b6d",
          },
          organisation: {
            alias: "broker2",
          },
        },
        url: "http://localhost:8290/organisations/broker2",
      };

      await o.post("/organisations", orgData, { expected: orgExpected });

      she("can find owned organization");

      await o.get(`/organisations?alias=${orgData.alias}`, {
        expected: {
          total: 1,
          limit: 5000,
          skip: 0,
          // "data": [{}]
        },
        ignoreKeyCompare: ["data"],
      });

      she("can find others organisation");

      await o.get(`/organisations?alias=carcosa`, {
        expected: {
          total: 1,
          limit: 5000,
          skip: 0,
          // "data": [{}]
        },
        ignoreKeyCompare: ["data"],
      });

      she("can get owned organsation");

      await o.get(`/organisations/${orgData.alias}`, {
        expected: orgExpected,
      });

      she("can get others organisation");

      await o.get(`/organisations/carcosa`, {
        expected: {
          _id: "a0d5a195-82d0-4bd1-b4c8-e55da3d653c9",
          owner: {
            user: { _id: "b1164adf-a440-416f-a0ae-b6a240aee39d" },
            organisation: { alias: "carcosa" },
          },
          changed: { by: "b1164adf-a440-416f-a0ae-b6a240aee39d", at: 1634037272956 },
          added: { by: "b1164adf-a440-416f-a0ae-b6a240aee39d", at: 1634037260707 },
          images: [],
          theme: { color: { secondary: "#cccccc", primary: "#666666" } },
          location: { longitude: null, latitude: null },
          visitAddress: {
            country: null,
            state: null,
            city: null,
            zipcode: null,
            coAddress: null,
            address: null,
          },
          description: { text: null, short: null },
          companyName: null,
          title: "Carcosa R&D",
          postAddress: {
            country: null,
            state: null,
            city: null,
            zipcode: null,
            coAddress: null,
            address: null,
          },
          contact: {
            phone: null,
            email: null,
            firstName: null,
            lastName: null,
          },
          website: null,
          phone: null,
          email: null,
          organisationNumber: null,
          name: "Carcosa R&D",
          alias: "carcosa",
          url: "http://localhost:8290/organisations/carcosa",
        },
      });

      she("can patch owned organisation");

      await o.patch(
        `/organisations/${orgData.alias}`,
        {
          phone: "3223311-444",
        },
        {
          expected: { ...orgExpected, phone: "3223311-444" },
        },
      );

      info("can not patch others organisation");

      await o.patch(
        `/organisations/carcosa`,
        {
          phone: "3223311-444",
        },
        {
          expected: {
            name: "NotFound",
            message: "No record found for id 'carcosa'",
            code: 404,
            className: "not-found",
            errors: {},
          },
        },
      );

      // she("can put owned organisation")

      // await o.put(`/organisations/${orgData.alias}`, orgData, { expected: orgExpected });

      info("can not put others organisation");

      await o.put(`/organisations/carcosa`, orgData, {
        expected: {
          name: "NotFound",
          message: "No record found for id 'carcosa'",
          code: 404,
          className: "not-found",
          errors: {},
        },
      });

      info("can not remove owned organisation");

      await o.remove(`/organisations/${orgData.alias}`, {
        expected: {
          name: "MethodNotAllowed",
          message: "Access denied remove organisations ",
          code: 405,
          className: "method-not-allowed",
          data: {
            path: "organisations",
            method: "remove",
          },
          errors: {},
        },
      });

      info("can not remove others organisation");

      await o.remove(`/organisations/carcosa`, {
        expected: {
          name: "MethodNotAllowed",
          message: "Access denied remove organisations ",
          code: 405,
          className: "method-not-allowed",
          data: {
            path: "organisations",
            method: "remove",
          },
          errors: {},
        },
      });

      she("can find owned permissions");

      await o.get(`/users/${standardUser1._id}/permissions`, {
        expected: {
          "organisations": ["get", "update", "patch", "create"],
          "organisations/:organisationAlias/grants": [
            "get",
            "find",
            "create",
            "update",
            "patch",
            "remove",
          ],
          "organisations/:organisationAlias/users": ["find"],
          "articles": ["*"],
          "users": ["get", "update", "patch"],
          "users/:userId/permissions": ["get", "find", "update", "patch"],
          "users/:userId/organisations": ["find"],
          "organisations/:organisationAlias/join": ["create"],
        },
      });

      she("can find others permissions");
      comment(
        "The userId in users/:userId/permissions is unused it will always return owned permission",
      );

      // TODO: Shouldnt have access to this
      await o.get(`/users/${standardUser2._id}/permissions`, {
        expected: {
          "organisations": ["get", "update", "patch", "create"],
          "organisations/:organisationAlias/grants": [
            "get",
            "find",
            "create",
            "update",
            "patch",
            "remove",
          ],
          "organisations/:organisationAlias/users": ["find"],
          "articles": ["*"],
          "users": ["get", "update", "patch"],
          "users/:userId/permissions": ["get", "find", "update", "patch"],
          "users/:userId/organisations": ["find"],
          "organisations/:organisationAlias/join": ["create"],
        },
      });

      she("can find owned organisation users/:userId/organisations (list) ");

      await o.get(`/users/${standardUser1._id}/organisations`, {
        expected: [
          {
            _id: "07f6f6e1-be96-4ac1-9af9-471016b64536",
            alias: "broker2",
            name: "Broker2 LTD.",
            title: "Broker2 LTD.",
          },
        ],
      });

      she("can find others organisation users/:userId/organisations (list) ");

      await o.get(`/users/${standardUser2._id}/organisations`, {
        expected: [],
      });

      await o.post(`/organisations/carcosa/grants`, { userId: standardUser2._id });

      await o.get(`/users/${standardUser2._id}/organisations`, {
        expected: [
          {
            _id: "67c43394-9a52-4be8-935c-cb8dfc7e5948",
            alias: "carcosa",
            name: "Carcosa R&D",
            title: "Carcosa R&D",
          },
        ],
      });
    });
  });
};
