import { axios, description, she, info } from "@lpgroup/import-cli";
import config from "./config.js";

export default async () => {
  return axios().then(async (ax) => {
    description("Grant service should not remove grant to self");
    const orgOwner = await ax.login(config.user, config.password);

    const expected = {
      _id: "d9e0b1cc-4396-4d5f-83f2-9ddf46a4a2c1",
      alias: "carcosa",
      name: "Carcosa R&D",
      title: "Carcosa R&D",
      images: [],
      theme: {
        color: {
          secondary: "#cccccc",
          primary: "#666666",
        },
      },
      location: {
        longitude: null,
        latitude: null,
      },
      visitAddress: {
        address: null,
        coAddress: null,
        zipcode: null,
        city: null,
        state: null,
        country: null,
      },
      description: {
        text: null,
        short: null,
      },
      companyName: null,
      postAddress: {
        address: null,
        coAddress: null,
        zipcode: null,
        city: null,
        state: null,
        country: null,
      },
      contact: {
        phone: null,
        email: null,
        firstName: null,
        lastName: null,
      },
      website: null,
      phone: null,
      email: null,
      organisationNumber: null,
      changed: {
        by: "8f7a39f4-8a78-4963-a049-591561fdeef0",
        at: 1614764095061,
      },
      added: {
        by: "8f7a39f4-8a78-4963-a049-591561fdeef0",
        at: 1614764095061,
      },
      owner: {
        user: {
          _id: "8f7a39f4-8a78-4963-a049-591561fdeef0",
        },
        organisation: {
          alias: "carcosa",
        },
      },
      url: "http://localhost:8290/organisations/d9e0b1cc-4396-4d5f-83f2-9ddf46a4a2c1",
    };

    she("orgOwner can get the organisation");
    await orgOwner.get(`/organisations/${config.alias}`, { expected });

    const expectedGrant = {
      userId: "daeb025c-480e-42bf-95fc-55ca7bd271fe",
      firstName: "Molly",
      lastName: "Millions",
      privilegie: "standard_organisation",
    };

    info("cleanup, remove grants for extraUser");
    const grant = await orgOwner.get(`/organisations/carcosa/grants/${config.extraUserId}`, {
      expected: {
        name: "NotFound",
        message: `No record found for id '${config.extraUserId}'`,
        code: 404,
        className: "not-found",
        errors: {},
      },
    });
    if (grant) {
      await orgOwner.remove(`/organisations/carcosa/grants/${grant.userId}`, {});
    }

    she("orgOwner can give grant to extraUser");
    const extraUserGrant = await orgOwner.post(
      `/organisations/${config.alias}/grants`,
      { userId: config.extraUserId },
      {
        expected: expectedGrant,
      },
    );

    info("cannot remove self grant");
    const otherUser = await ax.login("extra-onboarding@carcosa.se", config.password);
    await otherUser.remove(`/organisations/carcosa/grants/${extraUserGrant.userId}`, {
      expected: {
        name: "Forbidden",
        message: "Removing own grant is not allowed.",
        code: 403,
        className: "forbidden",
        errors: {},
      },
    });

    she("orgOwner can remove grant to extraUser");
    await orgOwner.remove(`/organisations/carcosa/grants/${extraUserGrant.userId}`, {
      expected: expectedGrant,
    });
  });
};
