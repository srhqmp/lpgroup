import { axios, description, she, comment } from "@lpgroup/import-cli";
import config from "./config.js";

export default async () => {
  return axios().then(async (ax) => {
    description("Sample Articles CRUD");
    const standardUser = await ax.login(config.user, config.password);

    const expected = {
      _id: "fde8de22-56a7-489b-b207-52e25867204f",
      tags: [{ _id: "3", text: "triangle" }],
      content: null,
      type: "NEWS",
      slug: null,
      url: "http://localhost:8290/articles/fde8de22-56a7-489b-b207-52e25867204f",
    };

    she("can create");
    const { _id: articleId } = await standardUser.post(
      `/articles`,
      { type: "NEWS", tags: [{ _id: "3", text: "triangle" }] },
      { expected },
    );

    she("can get");
    await standardUser.get(`/articles/${articleId}`, { expected });

    she("can patch primitive field");
    await standardUser.patch(
      `/articles/${articleId}`,
      { content: "Hello World" },
      { expected: { ...expected, content: "Hello World" } },
    );

    she("can patch an array field with an new _id");
    comment("If array contains elements with alias or _id merge that object.");
    await standardUser.patch(
      `/articles/${articleId}`,
      { tags: [{ _id: "4", text: "quadrilateral" }] },
      {
        expected: {
          ...expected,
          content: "Hello World",
          tags: [
            { _id: "3", text: "triangle" },
            { _id: "4", text: "quadrilateral" },
          ],
        },
      },
    );

    she("can patch an array field with same _id");
    comment("if item contains _id or alias with other field will update the item");
    await standardUser.patch(
      `/articles/${articleId}`,
      { tags: [{ _id: "3", text: "isoceles" }] },
      {
        expected: {
          ...expected,
          content: "Hello World",
          tags: [
            { _id: "3", text: "isoceles" },
            { _id: "4", text: "quadrilateral" },
          ],
        },
      },
    );

    she("can patch an array with _id only");
    comment("If item only contains _id or alias remove that item.");
    await standardUser.patch(
      `/articles/${articleId}`,
      { tags: [{ _id: "4" }] },
      {
        expected: {
          ...expected,
          content: "Hello World",
          tags: [{ _id: "3", text: "isoceles" }],
        },
      },
    );

    she("can patch an array without _id");
    comment("If one of the item in the array do not contain _id or alias, error");
    await standardUser.post(`/admin/log`, { message: "THIS SHOULD WRITE AN ERROR MESSAGE" });
    await standardUser.patch(
      `/articles/${articleId}`,
      { tags: [{ text: "pentagon" }] },
      {
        expected: {
          name: "GeneralError",
          message: "server error",
          code: 500,
          className: "general-error",
          errors: {},
        },
      },
    );
  });
};
