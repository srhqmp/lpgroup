/*
 dependencies: 20-organisations.js
*/
import { axios, description, she, info } from "@lpgroup/import-cli";
import config from "./config.js";

const user = {
  email: "usercascadedelete@carcosa.se",
  password: "TestingTesting@321",
  firstName: "Usercascade",
  lastName: "Delete",
  phone: "073-7654321",
};

const apiKeyExpected = {
  expected: {
    _id: "80faae34-14b7-42b9-bff4-c8ec83fb96ec",
    active: true,
    expires: 1638966420133,
    name: "Usercascade Delete",
    userId: "5f8ae15a-1f0b-4097-967d-31a533b7987f",
    changed: {
      by: "57c15cc1-08cc-4d32-880e-ff603e1e2ac6",
      at: 1638880024434,
    },
    added: {
      by: "57c15cc1-08cc-4d32-880e-ff603e1e2ac6",
      at: 1638880024434,
    },
    owner: {
      user: {
        _id: "57c15cc1-08cc-4d32-880e-ff603e1e2ac6",
      },
    },
    url: "http://localhost:8290/users/5f8ae15a-1f0b-4097-967d-31a533b7987f/api-keys/80faae34-14b7-42b9-bff4-c8ec83fb96ec",
  },
  ignoreKeyCompare: ["expires", "userId", "_id", "changed", "added", "url"],
};

export default async () => {
  return axios().then(async (ax) => {
    description("Simulate cascade functions when user is deleted");

    const superUser = await ax.login();

    info("cleanup");
    const targetUser = await superUser.get(`/users?email=${user.email}`);
    const id = targetUser.data[0] ? targetUser.data[0]._id : undefined;

    if (id) {
      info(`cleanup user, ${id}`);
      await superUser.remove(`/users/${id}`);
    }

    info("creates a user");
    const userObject = await ax.post("/onboarding/register-user", user, {
      expected: {
        _id: "42ccd57f-07dc-472d-b0e8-833b6912f8a2",
        active: true,
        type: "user",
        phone: "073-7654321",
        lastName: "Delete",
        firstName: "Usercascade",
        email: "usercascadedelete@carcosa.se",
        onlineStatus: "ONLINE",
        isOnline: false,
        profileImageUrl: null,
        oAuthStrategy: null,
        userAgent: null,
        ipNumber: null,
        verified: false,
        url: "http://localhost:8290/users/42ccd57f-07dc-472d-b0e8-833b6912f8a2",
      },
    });

    info("creates apiKey #1");
    const expires = new Date().setDate(new Date().getDate() + 1);
    await superUser.post(
      `/users/${userObject._id}/api-keys`,
      { apiKey: "KingKongDingDongPlingPlong@123", expires },
      apiKeyExpected,
    );

    info("creates apiKey #2");
    await superUser.post(
      `/users/${userObject._id}/api-keys`,
      { apiKey: "QueenDreamBeanIsVeryMean@123", expires },
      apiKeyExpected,
    );

    info("create apiKey #3");
    await superUser.post(
      `/users/${userObject._id}/api-keys`,
      { apiKey: "JackTheSlackIsNotWhack@123", expires },
      apiKeyExpected,
    );

    she("checks all created apiKeys");
    await superUser.get(`/users/${userObject._id}/api-keys`, {
      expected: {
        total: 3,
        limit: 5000,
        skip: 0,
        data: [],
      },
      ignoreKeyCompare: ["data"],
    });

    info("organisationCreator grants to an organisation");
    const organisationCreator = await ax.login(config.user, config.password);
    const userGrants = await organisationCreator.post(`/organisations/${config.alias}/grants`, {
      userId: userObject._id,
    });

    she(`checks the grant to ${config.alias} organisations`);
    await superUser.get(`/organisations/${config.alias}/grants/${userObject._id}`, {
      expected: {
        userId: userObject._id,
        firstName: "Usercascade",
        lastName: "Delete",
        privilegie: "standard_organisation",
        owner: {
          organisation: {
            alias: "carcosa",
          },
          user: {
            _id: "201e80a8-5531-42c6-beb9-6b18a462c465",
          },
        },
        changed: {
          by: "201e80a8-5531-42c6-beb9-6b18a462c465",
          at: 1638851143170,
        },
        added: {
          by: "201e80a8-5531-42c6-beb9-6b18a462c465",
          at: 1638851143170,
        },
        url: "http://localhost:8290/organisations/carcosa/grants/0846a9c9-6939-431c-a084-538794ad0deb",
      },
    });

    she("deletes the user to see cascade effect");
    await superUser.remove(`/users/${userObject._id}`, {
      expected: {
        _id: "8e4d7a05-a417-430a-b905-44fefa61055a",
        owner: {
          user: {
            _id: "8e4d7a05-a417-430a-b905-44fefa61055a",
          },
        },
        changed: {
          by: "superuser",
          at: 1638853717035,
        },
        added: {
          by: "superuser",
          at: 1638853717035,
        },
        active: true,
        type: "user",
        phone: user.phone,
        lastName: user.lastName,
        firstName: user.firstName,
        email: user.email,

        onlineStatus: "ONLINE",
        isOnline: false,
        profileImageUrl: null,
        oAuthStrategy: null,
        userAgent: null,
        ipNumber: null,
        verified: false,
        url: "http://localhost:8290/users/8e4d7a05-a417-430a-b905-44fefa61055a",
      },
    });

    she("should not return all apiKeys created.");
    await superUser.get(`/users/${userObject._id}/api-keys`, {
      expected: {
        total: 0,
        limit: 5000,
        skip: 0,
        data: [],
      },
      ignoreKeyCompare: ["data"],
    });

    she(`checks the grant to ${config.alias} organisations, should not be found`);
    await superUser.get(`/organisations/${config.alias}/grants/${userGrants._id}`, {
      expected: {
        name: "NotFound",
        message: "No record found for id 'undefined'",
        code: 404,
        className: "not-found",
        errors: {},
      },
    });
  });
};
