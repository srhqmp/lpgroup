/*
  dependencies: 13.create-api-key.js
*/

import { axios, description, info, she, comment } from "@lpgroup/import-cli";
import config from "./config.js";

export default async () => {
  return axios().then(async (ax) => {
    description("Standard User actions using apiKey as credentials to /organisation route");

    let userId;
    await ax.login().then(async (o) => {
      info("get ids");
      const user = await o.get(`/users?email=${config.user}`);
      userId = user.data[0]._id;
      if (!userId) throw new Error("User doesnt exist");

      info("cleanup API-key");
      await o.remove("/organisations/broker1", { ignoreError: true });
    });

    await ax.useApiKey(config.apiKey).then(async (o) => {
      const data = {
        alias: "broker1",
        name: "Broker1 LTD.",
        title: "Broker1 LTD.",
        organisationNumber: "202100-5489",
      };

      const expected = {
        _id: "cbdf6a2a-093c-4954-ad1d-6176a383a487",
        postAddress: {
          country: null,
          state: null,
          city: null,
          zipcode: null,
          coAddress: null,
          address: null,
        },
        contact: {
          phone: null,
          email: null,
          firstName: null,
          lastName: null,
        },
        website: null,
        phone: null,
        email: null,
        organisationNumber: "2021005489",
        name: "Broker1 LTD.",
        alias: "broker1",
        images: [],
        theme: {
          color: {
            secondary: "#cccccc",
            primary: "#666666",
          },
        },
        location: {
          longitude: null,
          latitude: null,
        },
        visitAddress: {
          country: null,
          state: null,
          city: null,
          zipcode: null,
          coAddress: null,
          address: null,
        },
        description: {
          text: null,
          short: null,
        },
        companyName: null,
        title: "Broker1 LTD.",
        changed: {
          by: "e7e05dcf-433c-4dae-ad97-5873a3d03b87",
          at: 1688355238552,
        },
        added: {
          by: "e7e05dcf-433c-4dae-ad97-5873a3d03b87",
          at: 1688355238552,
        },
        owner: {
          user: {
            _id: "e7e05dcf-433c-4dae-ad97-5873a3d03b87",
          },
          organisation: {
            alias: "broker1",
          },
        },
        url: "http://localhost:8290/organisations/broker1",
      };

      she("performs create with correct key");

      await o.post("/organisations", data, { expected });

      she("performs find with correct key");

      await o.get(`/organisations?alias=broker1`, {
        expected: {
          total: 1,
          limit: 5000,
          skip: 0,
          // "data": [{}]
        },
        ignoreKeyCompare: ["data"],
      });

      she("performs get with correct key");

      await o.get(`/organisations/broker1`, { expected });

      she("performs patch with correct key");

      await o.patch(
        "/organisations/broker1",
        { phone: "333-444" },
        { expected: { ...expected, phone: "333-444" } },
      );

      she("performs put with correct key");

      await o.put("/organisations/broker1", data, { expected });

      info("cannot performs remove with correct key");

      await o.remove("/organisations/broker1", {
        expected: {
          name: "MethodNotAllowed",
          message: "Access denied remove organisations ",
          code: 405,
          className: "method-not-allowed",
          data: {
            path: "organisations",
            method: "remove",
          },
          errors: {},
        },
      });

      comment("Note: find and get methods are 'public' for /organisation");
    });
  });
};
