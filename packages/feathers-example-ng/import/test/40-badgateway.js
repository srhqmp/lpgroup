import { axios, description, she } from "@lpgroup/import-cli";

export default async () => {
  return axios().then(async (ax) => {
    description("Bad gateway");

    she("should wait longer when server is overloaded, 'find'");
    await ax.get(`/badgateway`, {
      expected: {
        name: "BadGateway",
        message: "Error",
        code: 502,
        className: "bad-gateway",
        errors: {},
      },
    });

    she("should wait longer when server is overloaded, 'get'");
    await ax.get(`/badgateway/502`, {
      expected: {
        name: "BadGateway",
        message: "Error",
        code: 502,
        className: "bad-gateway",
        errors: {},
      },
    });

    she("should wait longer when server is overloaded, 'post'");
    await ax.post(
      `/badgateway`,
      { samplePayload: "isEmpty" },
      {
        expected: {
          name: "BadGateway",
          message: "Error",
          code: 502,
          className: "bad-gateway",
          errors: {},
        },
      },
    );

    she("should wait longer when server is overloaded, 'put'");
    await ax.put(
      `/badgateway/1`,
      { samplePayload: "isEmpty" },
      {
        expected: {
          name: "BadGateway",
          message: "Error",
          code: 502,
          className: "bad-gateway",
          errors: {},
        },
      },
    );

    she("should wait longer when server is overloaded, 'patch'");
    await ax.patch(
      `/badgateway/1`,
      { samplePayload: "isEmpty" },
      {
        expected: {
          name: "BadGateway",
          message: "Error",
          code: 502,
          className: "bad-gateway",
          errors: {},
        },
      },
    );
    she("should wait longer when server is overloaded, 'postPut'");
    await ax.postPut(`/badgateway`, "_id", { _id: "uniqueId", samplePayload: "isEmpty" });

    she("should wait longer when server is overloaded, 'remove'");
    await ax.remove(`/badgateway/1`, {
      expected: {
        name: "BadGateway",
        message: "Error",
        code: 502,
        className: "bad-gateway",
        errors: {},
      },
    });

    await ax.get(`/`);
  });
};
