/*
 dependencies: 11-onboarding-register-user.js
*/
import { axios, she, info, description, comment } from "@lpgroup/import-cli";
import onboardingUser from "./config.js";

export default async () => {
  description("Revoke token with 'delete' method on /authentication");
  return axios().then(async (ax) => {
    let userId;
    await ax.login().then(async (o) => {
      info("get Id");
      const user = await o.get(`/users?email=${onboardingUser.user}`);
      userId = user.data[0] && user.data[0]._id;
      if (!userId) throw new Error("User doesnt exist");
    });

    const expected = {
      _id: userId,
      owner: {
        user: {
          _id: userId,
        },
      },
      changed: {
        by: userId,
        at: 1634468968206,
      },
      added: {
        by: "superuser",
        at: 1634468925518,
      },
      active: true,
      type: "user",
      onlineStatus: "ONLINE",
      isOnline: false,
      profileImageUrl: null,
      oAuthStrategy: null,
      userAgent: "axios/1.6.8",
      ipNumber: "::ffff:127.0.0.1",
      verified: false,
      phone: "073-6265449",
      lastName: "Custer",
      firstName: "Reverend",
      email: "onboarding@carcosa.se",

      url: "http://localhost:8290/users/395610f0-367e-467b-a12f-e7feee353bb0",
    };

    await ax.login(onboardingUser.user, onboardingUser.password, true).then(async (o) => {
      she("can perform get with valid accessToken ");
      await o.get(`/users/${userId}`, {
        expected,
      });

      she("can revoke the token");
      await o.remove(`/authentication`, {
        expected: {
          accessToken: "token-details",
          user: {
            _id: userId,
            type: "user",
            firstName: "Reverend",
            lastName: "Custer",
            phone: "073-6265449",
            email: "onboarding@carcosa.se",
            verified: false,
            ipNumber: "::ffff:127.0.0.1",
            userAgent: "axios/1.6.8",
            isOnline: false,
            onlineStatus: "ONLINE",
            profileImageUrl: null,
            privileges: [
              {
                params: {
                  organisationAlias: null,
                  userId: "e7e05dcf-433c-4dae-ad97-5873a3d03b87",
                },
                privilegesAlias: "standard_user",
                _id: "b938bca1-8e3f-4599-bcac-ca292381d98d",
              },
              {
                _id: "8f7a2aba-6267-4f67-87af-c0d41cd8e9a0",
                privilegesAlias: "standard_organisation",
                params: {
                  userId: "e7e05dcf-433c-4dae-ad97-5873a3d03b87",
                  organisationAlias: "carcosa",
                },
              },
            ],
          },
        },
        noArraySort: true,
        ignoreKeyCompare: ["accessToken", "_id", "ipNumber"],
      });

      comment("Token is not revoked");
      she("can still perform get after revoke");
      await o.get(`/users/${userId}`, {
        // expected: {
        //   name: "NotAuthenticated",
        //   message: "Token revoked",
        //   code: 401,
        //   className: "not-authenticated",
        //   errors: {},
        // },
        expected,
      });
    });
  });
};
