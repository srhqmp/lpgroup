import { axios, description, she } from "@lpgroup/import-cli";

const testUserId = "c016e893-0c98-429e-9fe2-6096902f6250";
const testOrganisationAlias = "test-user-alias";

export default async () => {
  return axios().then(async (ax) => {
    description("Simulate register visitor device");
    const superUser = await ax.login();

    she("can cleanup db");
    await superUser.remove(`/users/${testUserId}`, { ignoreError: true });
    await superUser.remove(`/organisations/${testOrganisationAlias}`, {
      ignoreError: true,
    });

    she("create user");
    await ax.post("/onboarding/register-user", {
      _id: testUserId,
      email: "remove-test-user-id@carcosa.se",
      password: "Admin.Password@123",
      firstName: "test",
      lastName: "test",
      phone: "073-6265449",
    });

    const orgOwner = await ax.login("remove-test-user-id@carcosa.se", "Admin.Password@123");

    she("Create organisation");
    await orgOwner.post("/organisations", {
      alias: testOrganisationAlias,
      name: "Test LTD",
      title: "Test LTD",
    });

    she("superUser can delete org");
    await superUser.remove(`/organisations/${testOrganisationAlias}`);

    she("Create organisation");
    await orgOwner.post("/organisations", {
      alias: testOrganisationAlias,
      name: "Test LTD",
      title: "Test LTD",
    });

    she("test cascade delete of grants on organisations");
    await superUser.remove(`/users/${testUserId}`);
  });
};
