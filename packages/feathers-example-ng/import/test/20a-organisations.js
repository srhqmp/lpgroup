import { axios, she, description } from "@lpgroup/import-cli";
import config from "./config.js";

const { userId } = config;
export const extraUserId = "daeb025c-480e-42bf-95fc-55ca7bd271fe";

export default async () => {
  return axios().then(async (ax) => {
    description("Test organisation, permissions and grants");

    await ax.login().then(async (o) => {
      await o.remove(`/users/${userId}`, { ignoreError: true });
      await o.remove(`/users/${extraUserId}`, { ignoreError: true });
      await o.remove("/organisations/carcosa", { ignoreError: true });
    });

    she("create user");
    await ax.post("/onboarding/register-user", {
      _id: userId,
      email: config.user,
      password: config.password,
      firstName: "Reverend",
      lastName: "Custer",
      phone: "073-6265449",
    });

    await ax.post("/onboarding/register-user", {
      _id: extraUserId,
      email: "extra-onboarding@carcosa.se",
      password: config.password,
      firstName: "Molly",
      lastName: "Millions",
      phone: "073-6265449",
    });

    await ax.login(config.user, config.password, true).then(async (o) => {
      const expected = {
        _id: "d9e0b1cc-4396-4d5f-83f2-9ddf46a4a2c1",
        alias: "carcosa",
        name: "Carcosa R&D",
        title: "Carcosa R&D",
        images: [],
        theme: {
          color: {
            secondary: "#cccccc",
            primary: "#666666",
          },
        },
        location: {
          longitude: null,
          latitude: null,
        },
        visitAddress: {
          address: null,
          coAddress: null,
          zipcode: null,
          city: null,
          state: null,
          country: null,
        },
        description: {
          text: null,
          short: null,
        },
        companyName: null,
        postAddress: {
          address: null,
          coAddress: null,
          zipcode: null,
          city: null,
          state: null,
          country: null,
        },
        contact: {
          phone: null,
          email: null,
          firstName: null,
          lastName: null,
        },
        website: null,
        phone: null,
        email: null,
        organisationNumber: null,
        changed: {
          by: "8f7a39f4-8a78-4963-a049-591561fdeef0",
          at: 1614764095061,
        },
        added: {
          by: "8f7a39f4-8a78-4963-a049-591561fdeef0",
          at: 1614764095061,
        },
        owner: {
          user: {
            _id: "8f7a39f4-8a78-4963-a049-591561fdeef0",
          },
          organisation: {
            alias: "carcosa",
          },
        },
        url: "http://localhost:8290/organisations/d9e0b1cc-4396-4d5f-83f2-9ddf46a4a2c1",
      };

      she("create organisation, with new user");
      // userId will get standard_organisation access when creating the organisation
      await o.post(
        "/organisations",
        {
          alias: "carcosa",
          name: "Carcosa R&D",
          title: "Carcosa R&D",
        },
        { expected },
      );

      await o.get("/organisations");
      await o.get("/organisations/carcosa", { expected });

      await o.get(`/users/${userId}`, {
        expected: {
          _id: userId,
          type: "user",
          active: true,
          phone: "073-6265449",
          lastName: "Custer",
          firstName: "Reverend",
          email: "onboarding@carcosa.se",

          onlineStatus: "ONLINE",
          isOnline: false,
          profileImageUrl: null,
          oAuthStrategy: null,
          userAgent: "axios/1.6.8",
          ipNumber: "::ffff:127.0.0.1",
          verified: false,
        },
      });

      await o.get("/organisations/cafe-not-exist", {
        expected: {
          name: "NotFound",
          message: "No record found for id 'cafe-not-exist'",
          code: 404,
          className: "not-found",
          errors: {},
        },
      });

      await o.patch(
        "/organisations/carcosa",
        {
          phone: "123-456",
        },
        {
          expected: { ...expected, phone: "123-456" },
        },
      );

      await o.put(
        "/organisations/carcosa",
        {
          alias: "carcosa",
          name: "Carcosa R&D",
          title: "Carcosa R&D",
        },
        {
          expected: { ...expected },
        },
      );

      const standardOrganisationExpected = {
        "organisations": ["get", "update", "patch", "create"],
        "organisations/:organisationAlias/grants": [
          "get",
          "find",
          "create",
          "update",
          "patch",
          "remove",
        ],
        "users": ["get", "update", "patch"],
        "users/:userId/permissions": ["get", "find", "update", "patch"],
        "users/:userId/organisations": ["find"],
        "organisations/:organisationAlias/join": ["create"],
        "organisations/:organisationAlias/users": ["find"],
        "articles": ["*"],
      };
      await o.get(`/users/${userId}/permissions`, {
        expected: standardOrganisationExpected,
      });

      she("grant organisation permissions to extra user");
      await o.post(`/organisations/carcosa/grants`, { userId: extraUserId });
      await o.get(`/users/${extraUserId}/permissions`, {
        expected: standardOrganisationExpected,
      });
      await o.get(`/users/${extraUserId}/organisations`, {
        expected: [
          {
            _id: "27043799-413f-4433-8040-20d8e9b9ce68",
            alias: "carcosa",
            name: "Carcosa R&D",
            title: "Carcosa R&D",
          },
        ],
      });

      await o.get(`/organisations/carcosa/users`, {
        expected: [
          {
            _id: "e7e05dcf-433c-4dae-ad97-5873a3d03b87",
            type: "user",
            profileImageUrl: null,
            onlineStatus: "ONLINE",
            oAuthStrategy: null,
            isOnline: false,
            phone: "073-6265449",
            lastName: "Custer",
            firstName: "Reverend",
            email: "onboarding@carcosa.se",
            active: true,
          },
          {
            _id: "daeb025c-480e-42bf-95fc-55ca7bd271fe",
            type: "user",
            profileImageUrl: null,
            onlineStatus: "ONLINE",
            oAuthStrategy: null,
            isOnline: false,
            phone: "073-6265449",
            lastName: "Millions",
            firstName: "Molly",
            email: "extra-onboarding@carcosa.se",
            active: true,
          },
        ],
        noArraySort: true,
      });

      const grants = await o.get(`/organisations/carcosa/grants`, {
        expected: {
          total: 2,
          limit: 5000,
          skip: 0,
          data: [
            {
              userId: "e7e05dcf-433c-4dae-ad97-5873a3d03b87",
              firstName: "Reverend",
              lastName: "Custer",
              privileges: [
                {
                  params: {
                    organisationAlias: null,
                    userId: "e7e05dcf-433c-4dae-ad97-5873a3d03b87",
                  },
                  privilegesAlias: "standard_user",
                  _id: "e3186d30-8002-4a2f-88d5-5fba84635677",
                },
                {
                  _id: "ca091a3a-5e44-4a71-b6c6-e5763c8bb047",
                  privilegesAlias: "standard_organisation",
                  params: {
                    userId: "e7e05dcf-433c-4dae-ad97-5873a3d03b87",
                    organisationAlias: "carcosa",
                  },
                },
              ],
            },
            {
              userId: "daeb025c-480e-42bf-95fc-55ca7bd271fe",
              firstName: "Molly",
              lastName: "Millions",
              privileges: [
                {
                  params: {
                    organisationAlias: null,
                    userId: "daeb025c-480e-42bf-95fc-55ca7bd271fe",
                  },
                  privilegesAlias: "standard_user",
                  _id: "cdd582c1-02a9-421c-a77b-6f4158bedede",
                },
                {
                  _id: "4fd6af14-75bd-4bd7-9e96-a5c7394c5f34",
                  privilegesAlias: "standard_organisation",
                  params: {
                    userId: "daeb025c-480e-42bf-95fc-55ca7bd271fe",
                    organisationAlias: "carcosa",
                  },
                },
              ],
            },
          ],
        },
        noArraySort: true,
      });

      const grant = grants.data.filter((v) => v.userId === extraUserId)[0];
      await o.remove(`/organisations/carcosa/grants/${grant.userId}`);
      await o.get(`/organisations/carcosa/users`, {
        expected: [
          {
            _id: "e7e05dcf-433c-4dae-ad97-5873a3d03b87",
            type: "user",
            profileImageUrl: null,
            onlineStatus: "ONLINE",
            oAuthStrategy: null,
            isOnline: false,
            phone: "073-6265449",
            lastName: "Custer",
            firstName: "Reverend",
            email: "onboarding@carcosa.se",
            active: true,
          },
        ],
      });
    });
  });
};
