import { axios, description, info, she } from "@lpgroup/import-cli";

const userCreds = {
  userId: "7354bd1f-72a3-4bd4-a3d9-2231df2f7fb9",
  email: "uniqueuser@carcosa.se",
  password: "Very-Unique-Password@123",
};

export default async () => {
  return axios().then(async (ax) => {
    description("Preform user escalate privilege");
    const superUser = await ax.login();

    info("cleanup");
    await superUser.remove(`/users/${userCreds.userId}`, { ignoreError: true });

    info("should gets error if privileges field is provided in create");
    await ax.post(
      "/onboarding/register-user",
      {
        _id: userCreds.userId,
        email: userCreds.email,
        password: userCreds.password,
        firstName: "User",
        lastName: "Pentester",
        phone: "073-6265449",
        privileges: [
          {
            params: { organisationAlias: null, userId: null },
            privilegesAlias: "super_user",
            _id: "123-456-7890",
          },
        ],
      },
      {
        expected: {
          name: null,
          message: "this field has unspecified keys: privileges",
          code: 400,
          className: "ValidationError",
          data: {},
          errors: [
            {
              message: "this field has unspecified keys: privileges",
              path: "",
              value: {
                _id: "7354bd1f-72a3-4bd4-a3d9-2231df2f7fb9",
                email: "uniqueuser@carcosa.se",
                password: "Very-Unique-Password@123",
                firstName: "User",
                lastName: "Pentester",
                phone: "073-6265449",
                privileges: [
                  {
                    params: {
                      organisationAlias: null,
                      userId: null,
                    },
                    privilegesAlias: "super_user",
                    _id: "123-456-7890",
                  },
                ],
              },
            },
          ],
        },
      },
    );

    she("creates as regular user");
    await ax.post(
      "/onboarding/register-user",
      {
        _id: userCreds.userId,
        email: userCreds.email,
        password: userCreds.password,
        firstName: "User",
        lastName: "Pentester",
        phone: "073-6265449",
      },
      {
        expected: {
          _id: "7354bd1f-72a3-4bd4-a3d9-2231df2f7fb9",
          type: "user",
          profileImageUrl: null,
          onlineStatus: "ONLINE",
          oAuthStrategy: null,
          isOnline: false,
          userAgent: null,
          ipNumber: null,
          verified: false,
          phone: "073-6265449",
          lastName: "Pentester",
          firstName: "User",
          email: "uniqueuser@carcosa.se",

          active: true,
          url: "http://localhost:8290/users/7354bd1f-72a3-4bd4-a3d9-2231df2f7fb9",
        },
      },
    );

    she("logins with email and password");
    const regularUser = await ax.login(userCreds.email, userCreds.password);

    const adminResult = {
      truncate_database_url: "/admin/truncate/database",
    };
    she("able to get admin/test as superUser");
    await superUser.get("/admin", { expected: adminResult });

    info("should fail to get admin as regular user");
    await regularUser.get("/admin", {
      expected: {
        name: "MethodNotAllowed",
        message: "Access denied find admin ",
        code: 405,
        className: "method-not-allowed",
        data: {
          path: "admin",
          method: "find",
        },
        errors: {},
      },
    });

    const userResult = {
      _id: "7354bd1f-72a3-4bd4-a3d9-2231df2f7fb9",
      type: "user",
      profileImageUrl: null,
      onlineStatus: "ONLINE",
      oAuthStrategy: null,
      isOnline: false,
      userAgent: "axios/1.6.8",
      ipNumber: "::ffff:127.0.0.1",
      verified: false,
      phone: "073-6265449",
      lastName: "Pentester",
      firstName: "User",
      email: "uniqueuser@carcosa.se",

      owner: {
        user: {
          _id: "7354bd1f-72a3-4bd4-a3d9-2231df2f7fb9",
        },
      },
      changed: {
        by: "superuser",
        at: 1662942621315,
      },
      added: {
        by: "superuser",
        at: 1662942620798,
      },
      active: true,
      url: "http://localhost:8290/users/7354bd1f-72a3-4bd4-a3d9-2231df2f7fb9",
    };

    she("gets own identity");
    await regularUser.get(`/users/${userCreds.userId}`, { expected: userResult });

    info("should fail to escalate privilege using patch");
    await regularUser.patch(
      `/users/${userCreds.userId}`,
      {
        privileges: [
          {
            params: { organisationAlias: null, userId: null },
            privilegesAlias: "super_user",
            _id: "123-456-7890",
          },
        ],
      },
      {
        expected: {
          name: "MethodNotAllowed",
          message: "Provider 'rest' can not call 'patch'. (disallow)",
          code: 405,
          className: "method-not-allowed",
        },
      },
    );

    she("can do basic patch");
    await regularUser.patch(
      `/users/${userCreds.userId}`,
      { firstName: "Cloe" },
      { expected: { ...userResult, firstName: "Cloe" } },
    );

    info("should fail perform admin privilege");
    await regularUser.get("/admin", {
      expected: {
        name: "MethodNotAllowed",
        message: "Access denied find admin ",
        code: 405,
        className: "method-not-allowed",
        data: { path: "admin", method: "find" },
        errors: {},
      },
    });

    info("should fail to escalate privilege using put");
    await regularUser.put(
      `/users/${userCreds.userId}`,
      {
        _id: "7354bd1f-72a3-4bd4-a3d9-2231df2f7fb9",
        type: "user",
        profileImageUrl: null,
        onlineStatus: "ONLINE",
        oAuthStrategy: null,
        isOnline: false,
        userAgent: "axios/1.6.8",
        ipNumber: "::ffff:127.0.0.1",
        verified: false,
        phone: "073-6265449",
        lastName: "Pentester",
        firstName: "User",
        email: "uniqueuser@carcosa.se",
        privileges: [
          {
            params: { organisationAlias: null, userId: null },
            privilegesAlias: "super_user",
            _id: "123-456-7890",
          },
        ],
      },
      {
        expected: {
          name: "MethodNotAllowed",
          message: "Provider 'rest' can not call 'update'. (disallow)",
          code: 405,
          className: "method-not-allowed",
        },
      },
    );

    info("should fail perform admin privilege");
    await regularUser.get("/admin", {
      expected: {
        name: "MethodNotAllowed",
        message: "Access denied find admin ",
        code: 405,
        className: "method-not-allowed",
        data: {
          path: "admin",
          method: "find",
        },
        errors: {},
      },
    });
  });
};
