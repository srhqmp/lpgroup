/*
  dependencies: load.js, 11-onboarding-resgister-user.js
*/

import { axios, description, info, she } from "@lpgroup/import-cli";
import config from "./config.js";

const apiKeyId = "36a5c18f-2422-46db-9868-cc469bb72054";

export default async () => {
  return axios().then(async (ax) => {
    description("Creating API-key");

    let userId;
    await ax.login().then(async (o) => {
      info("get ids");
      const user = await o.get(`/users?email=${config.user}`);
      userId = user.data[0]._id;
      if (!userId) throw new Error("User doesnt exist");

      info("cleanup API-key");
      await o.remove(`/users/${userId}/api-keys/${apiKeyId}`, { ignoreError: true });
    });

    info("cannot create when user is not authenticated");

    await ax.post(
      `/users/${userId}/api-keys`,
      {
        apiKey: config.apiKey,
      },
      {
        expected: {
          name: "MethodNotAllowed",
          message: "Access denied create users/:userId/api-keys ",
          code: 405,
          className: "method-not-allowed",
          data: {
            path: "users/:userId/api-keys",
            method: "create",
          },
          errors: {},
        },
      },
    );

    info("cannot create when Login but not authorized");

    await ax.login(config.user, config.password).then(async (o) => {
      await o.post(
        `/users/${userId}/api-keys`,
        {
          apiKey: config.apiKey,
        },
        {
          expected: {
            name: "MethodNotAllowed",
            message: "Access denied create users/:userId/api-keys ",
            code: 405,
            className: "method-not-allowed",
            data: {
              path: "users/:userId/api-keys",
              method: "create",
            },
            errors: {},
          },
        },
      );
    });

    she("can create as super user (JWT)");
    const current = new Date();
    await ax.login().then(async (o) => {
      await o.post(
        `/users/${userId}/api-keys`,
        {
          _id: apiKeyId,
          name: "test key",
          apiKey: config.apiKey,
          // one day
          expires: current.setDate(current.getDate() + 1),
        },
        {
          expected: {
            _id: apiKeyId,
            active: true,
            name: "test key",
            expires: 1634469390386,
            userId,
            changed: {
              by: "57c15cc1-08cc-4d32-880e-ff603e1e2ac6",
              at: 1634382992164,
            },
            added: {
              by: "57c15cc1-08cc-4d32-880e-ff603e1e2ac6",
              at: 1634382992164,
            },
            owner: {
              user: {
                _id: "57c15cc1-08cc-4d32-880e-ff603e1e2ac6",
              },
            },
            url: `http://localhost:8290/users/${userId}/api-keys/${apiKeyId}`,
          },
          ignoreKeyCompare: ["expires", "changed", "added"],
        },
      );
      info("throws a General Error if apiKey is already created");

      await o.post(
        `/users/${userId}/api-keys`,
        {
          name: "test key",
          apiKey: config.apiKey,
        },
        {
          expected: {
            name: "GeneralError",
            message:
              'E11000 duplicate key error collection: feathers-example-ng-dev.users-api-keys index: apiKey_1 dup key: { apiKey: "4c7e8314440087833d54f53c4b5c6570517bdaa566e56562ef1eb30302dabe6b" }',
            code: 500,
            className: "general-error",
            data: {},
            errors: {},
          },
          // message contains generated apiKey
          ignoreKeyCompare: ["message"],
        },
      );
    });

    await ax.useApiKey("wrongapikey").then(async (o) => {
      info("throws NotAuthenticated error if apiKey is provided but not found in database");

      await o.post(
        `/users/${userId}/api-keys`,
        {
          apiKey: config.apiKey,
        },
        {
          expected: {
            name: "NotAuthenticated",
            message: "Incorrect API Key",
            code: 401,
            className: "not-authenticated",
            errors: {},
          },
        },
      );
    });

    await ax.useApiKey(config.apiKey).then(async (o) => {
      she("can get user with apiKey");

      await o.get(`/users/${userId}`, {
        expected: {
          _id: userId,
          owner: {
            user: {
              _id: userId,
            },
          },
          changed: {
            by: userId,
            at: 1634441164871,
          },
          added: {
            by: "superuser",
            at: 1634441159055,
          },
          active: true,
          type: "user",
          phone: "123 456 789",
          lastName: "Custer",
          firstName: "Reverend",
          email: "onboarding@carcosa.se",

          onlineStatus: "ONLINE",
          isOnline: false,
          profileImageUrl: null,
          oAuthStrategy: null,
          userAgent: "axios/1.6.8",
          ipNumber: "::ffff:127.0.0.1",
          verified: false,
          url: `http://localhost:8290/users/${userId}`,
        },
      });
    });
  });
};
