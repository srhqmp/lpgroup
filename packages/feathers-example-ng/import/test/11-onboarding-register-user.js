import { axios, info } from "@lpgroup/import-cli";
import config from "./config.js";

export default async () => {
  return axios().then(async (ax) => {
    await ax.login().then(async (o) => {
      await o.remove(`/users/${config.userId}`, { ignoreError: true });
    });

    info("fail to board using simplepassword");
    await ax.post(
      "/onboarding/register-user",
      {
        _id: config.userId,
        email: config.user,
        password: "simplepassword",
        firstName: "Reverend",
        lastName: "Custer",
        phone: "073-6265449",
      },
      {
        expected: {
          name: null,
          message:
            "Password must contain at least one number, one special character, one capital letter, and be at least 15 characters long.",
          code: 400,
          className: "ValidationError",
          data: {},
          errors: [
            {
              message:
                "Password must contain at least one number, one special character, one capital letter, and be at least 15 characters long.",
              path: "password",
              value: "simplepassword",
            },
          ],
        },
      },
    );

    await ax.post(
      "/onboarding/register-user",
      {
        _id: config.userId,
        email: config.user,
        password: config.password,
        firstName: "Reverend",
        lastName: "Custer",
        phone: "073-6265449",
      },
      {
        expected: {
          _id: config.userId,
          email: config.user,

          firstName: "Reverend",
          lastName: "Custer",
          phone: "073-6265449",
          active: true,
          type: "user",
          onlineStatus: "ONLINE",
          isOnline: false,
          userAgent: null,
          ipNumber: null,
          verified: false,
          profileImageUrl: null,
          oAuthStrategy: null,
          url: "http://localhost:8290/users/d3b7b6d0-80c9-4304-bcc0-7238c98ee549",
        },
      },
    );
  });
};
