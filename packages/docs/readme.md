# @lpgroup/docs

MDX parser library that returns a styled JSX.

## Contents

- [Getting Started](#getting-started)
- [Installation](#installation)
- [MDX Setup](#mdx-setup)

## Getting Started

The objective of this library is to parse MDX files and convert them into a ready-made _'documentation format'_.

```sh
yarn create vite
```

## Installation

To install this library, we can run in the command line.

```sh
# via yarn
yarn add @lpgroup/docs

# via npm
npm install --save @lpgroup/docs

```

# MDX Setup

```
import React from "react";
import LPDocs, { DocsSkeleton } from "@lpgroup/docs";
import { SampleLogo, Footer } from "./custom/SampleLogo";
const systemFont = `Raleway, Lato, -apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", sans-serif`;

const typhographySettings = {
  h1: {
    fontSize: "2.2rem",
    fontWeight: 600,
    fontFamily: systemFont,
    color: "#3cf",
  },
  h2: {
    fontSize: "1.5rem",
    fontWeight: 500,
    fontFamily: systemFont,
  },

  h3: {
    fontSize: "1.3rem",
    fontWeight: 500,
    fontFamily: systemFont,
  },
  h4: {
    fontSize: "1rem",
    fontWeight: 500,
  },
  body1: {
    lineHeight: 1.7,
    fontFamily: systemFont,
  },
  body2: {
    "fontSize": 15,
    "fontFamily": systemFont,
    "&:hover": {
      color: "#6969ff",
    },
  },
};

const Pages = React.lazy(() => import("./pages/v1"));

function App() {
  return (
    <LPDocs
      Logo={SampleLogo}
      Footer={Footer}
      sideBarHeight={60}
      isAuth={true}
      font={systemFont}
      textAlign={"justify"}
      typhography={typhographySettings}
    >
      <React.Suspense fallback={<DocsSkeleton />}>
        <Pages />
      </React.Suspense>
    </LPDocs>
  );
}

export default App;


```

#### Contribute

See [contribute](https://gitlab.com/lpgroup/lpgroup)

#### License

MIT - See [licence](https://gitlab.com/lpgroup/lpgroup/-/blob/master/LICENSE.md)
