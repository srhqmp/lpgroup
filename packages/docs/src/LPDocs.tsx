import * as React from "react";
import { useState } from "react";
import { DocsTheme, ScrollToTop, UserProvider } from "./components";
import { Sidebar } from "./components";
import { TopBar } from "./components";
import { MDXWrap } from "./components/content/MDXWrap";
import { useTheme, useMediaQuery } from "@mui/material";
import "./style.css";

type TypographyOptions = import("@mui/material/styles/createTypography").TypographyOptions;

type TextAlign = "center" | "end" | "justify" | "left" | "match-parent" | "right" | "start";

interface PropsDocs {
  sideBarWidth?: { md: number; lg: number; xl: number };
  Logo?: React.FC;
  children: React.ReactNode;
  Footer?: React.FC;
  typography?: TypographyOptions;
  font?: string;
  isAuth?: boolean;
  textAlign?: TextAlign;
  sideBarHeight?: number;
  sxTopBar: React.CSSProperties;
}

export const LPDocs = (props: PropsDocs) => {
  const {
    sideBarWidth = { md: 256, lg: 290, xl: 310 },
    Logo = LPGroupLogo,
    font = "",
    children,
    Footer,
    typography,
    isAuth = false,
    textAlign,
    sideBarHeight = 49,
    sxTopBar = {},
  } = props;
  const [openSidebar, setOpenSidebar] = useState(false);
  const theme = useTheme();
  const isMd = useMediaQuery(theme.breakpoints.up("md"), {
    defaultMatches: true,
  });
  const open = isMd ? false : openSidebar;

  const onSidebarOpen = () => {
    setOpenSidebar(true);
  };
  const handleSidebarClose = () => {
    setOpenSidebar(false);
  };

  return (
    <DocsTheme typography={typography} font={font} textAlign={textAlign}>
      <UserProvider>
        <TopBar sxTopBar={sxTopBar} Logo={Logo} onSidebarOpen={onSidebarOpen} isAuth={isAuth} />
        <Sidebar
          onClose={handleSidebarClose}
          open={open}
          variant={isMd ? "permanent" : "temporary"}
          sideBarWidth={sideBarWidth}
          sideBarHeight={sideBarHeight}
        />
        <MDXWrap sideBarWidth={sideBarWidth} Footer={Footer}>
          {children}
        </MDXWrap>
        <ScrollToTop />
      </UserProvider>
    </DocsTheme>
  );
};

export default LPDocs;

const LPGroupLogo = () => <div>@LPGROUP</div>;
