import * as React from "react";
import { Box, Divider } from "@mui/material";
import { MDXProvider } from "@mdx-js/react";
import { mdxComponents } from "./mdxComponents";

export const MDXWrap = ({ children, sideBarWidth: sw, Footer }: any) => {
  return (
    <Box paddingLeft={{ md: `${sw.md}px`, lg: `${sw.lg}px`, xl: `${sw.xl}px` }}>
      <Box display="flex" flex="1 1 auto" overflow="hidden">
        <Box flex="1 1 auto" height="100%" overflow="auto" mx="auto">
          <Box px={4} width={{ sx: "100%" }}>
            <Box id="top-page" minHeight={{ xs: 400, md: 800 }} paddingTop={3} paddingX={{ md: 2 }}>
              <MDXProvider arial-label="mdx-content" components={mdxComponents}>
                {children}
              </MDXProvider>
            </Box>
          </Box>
          {Footer && <Footer />}
          <Divider />
        </Box>
      </Box>
    </Box>
  );
};
