import * as React from "react";
import { Box, Divider, Typography } from "@mui/material";

export const Attributes = (props: any) => {
  return (
    <>
      {props.children}
      <br />
    </>
  );
};
export const Attribute = (props: any) => {
  return (
    <Box>
      <Divider flexItem sx={{ my: 1 }} />
      <Box display="flex">
        <Typography pr={2} sx={{ fontWeight: "bold" }}>
          {props.title}
        </Typography>
        <Typography color={"GrayText"}>{props.type}</Typography>
      </Box>
      <div style={{ textAlign: "justify" }}>{props.children.props.children}</div>
    </Box>
  );
};
