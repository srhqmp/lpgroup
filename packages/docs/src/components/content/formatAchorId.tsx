export const formatAnchorId = (title: string | any) => {
  return title.replace(/[\W]+/g, "_").toLowerCase() || "#";
};
