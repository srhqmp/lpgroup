import { responsiveFontSizes, createTheme } from "@mui/material";
import { shadows } from "./shadows";
import { light, dark } from "./palette";

export const getTheme = (
  mode: string | boolean | (() => void),
  themeToggler: string | boolean | (() => void),
) =>
  responsiveFontSizes(
    createTheme({
      // @ts-ignore
      palette: mode === "light" ? light : dark,
      // @ts-ignore
      shadows: shadows(mode),
      typography: {
        fontFamily: '"Inter", sans-serif',
        button: {
          textTransform: "none",
          fontWeight: "medium",
        },
      },
      zIndex: {
        appBar: 1200,
        drawer: 1300,
      },
      components: {
        MuiButton: {
          styleOverrides: {
            root: {
              fontWeight: 400,
              borderRadius: 5,
              paddingTop: 10,
              paddingBottom: 10,
            },
            containedSecondary: mode === "light" ? { color: "white" } : {},
          },
        },
        MuiInputBase: {
          styleOverrides: {
            root: {
              borderRadius: 5,
            },
          },
        },
        MuiOutlinedInput: {
          styleOverrides: {
            root: {
              borderRadius: 5,
            },
            input: {
              borderRadius: 5,
            },
          },
        },
        MuiCard: {
          styleOverrides: {
            root: {
              borderRadius: 8,
            },
          },
        },
        MuiListItemText: {
          styleOverrides: { root: { marginBlock: "2px" } },
        },
      },
      themeToggler,
    }),
  );
