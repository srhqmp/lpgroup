# @lpgroup/feathers-counters-service

[![npm version](https://badge.fury.io/js/%40lpgroup%2Ffeathers-counters-service.svg)](https://badge.fury.io/js/%40lpgroup%2Ffeathers-counters-service) [![Known Vulnerabilities](https://snyk.io/test/npm/@lpgroup/feathers-counters-service/badge.svg)](https://snyk.io/test/npm/@lpgroup/feathers-counters-service)[![Licence MIT](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/lpgroup/lpgroup/-/blob/master/LICENSE.md)
[![tested with jest](https://img.shields.io/badge/tested_with-jest-99424f.svg)](https://github.com/facebook/jest) [![codecov](https://codecov.io/gl/lpgroup/lpgroup/branch/master/graph/badge.svg?token=RRZ6GDUQXT)](https://codecov.io/gl/lpgroup/lpgroup)

Feathersjs hook that adds a counter key to data or result.

The package consists of the service `counters` and the hook `counter`. The hook will store the number of times a specific event has occured in a database through the service.

This funcionality can for example be used for statistics purpose. Counting the number of times `GET` has occured on a specific endpoint. Number of times a specific user has accessed a service. It can also be used to calculate a unique customer number on a customer endpoint on a specific organisation.

## Install

Installation of the npm

```sh
npm install @lpgroup/feathers-counters-service
// or
yarn add @lpgroup/feathers-counters-service
```

## The hook

| **before** | **after** | **methods** | **multi** | **source**                                                                                                         |
| ---------- | --------- | ----------- | --------- | ------------------------------------------------------------------------------------------------------------------ |
| yes        | yes       | all         | yes       | [source](https://gitlab.com/lpgroup/lpgroup/-/blob/master/packages/feathers-counters-service/src/hooks/counter.js) |

### arguments

| property      | type     | default  | description                                                                            |
| ------------- | -------- | -------- | -------------------------------------------------------------------------------------- |
| `format`      | `string` | required | A format that makes the counter unique                                                 |
| `periodFmt`   | `string` | required | The date period that the counter is counting. https://date-fns.org/v2.28.0/docs/format |
| `key`         | `string` | true     | The name of the key in the returned json data/result                                   |
| `step`        | `number` | 1        | Number of steps to increase the counter each time, postive or negative.                |
| `description` | `string` | null     | Description of the counter                                                             |

## The service

The service is required by the hook. To install it, modify feathers service file, `app-ng/src/services/index.js`, add the follwoing.

```js
import { counters } from "@lpgroup/feathers-counters-service";

export default (app) => {
  /* ... */
  app.configure(counters);
};
```

### Endpoints

```bash
POST: /counters
GET: /counters
GET: /counters/:alias
PATCH: /counters/:alias
PUT: /counters/:alias
DELETE: /counters/:alias
```

### Object

```json
{
  "alias": "status-visits-2021-11",
  "group": "status-visits",
  "period": "2021-11",
  "value": 50,
  "description": "status-visits"
  /* commonAttributes */
}
```

## Example 1

Give each customer on an organisation a unique customer number, on the endpoint `/organisations/:organisationAlias/customer`.

The before hook will add the key customerNumber to `data` and store it in the database handled by the endpoint.

`{organisationAlias}` comes internally from the url in `params.query`.

```js
import { counter } from "@lpgroup/feathers-counters-service";

export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [
      counter("{organisationAlias}-visitors", null, "customerNumber", 1, "Latest customerNumber"),
    ],
    update: [],
    patch: [],
    remove: [],
  },
};
```

## Example 2

Calculate the number of times a user has accessed the GET method on `/status?user=admin`.

`{user}` comes internally from the query parameter in `params.query`.

```js
import { counter } from "@lpgroup/feathers-counters-service";

export default {
  after: {
    all: [],
    find: [],
    get: [
      counter(
        "total_gets-{user}",
        null,
        "userAccesses",
        1,
        "Total number of calls done by a specific user",
      ),
    ],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
```

## Example 3

Calculate number of calls during a month.

The counter will start from the beginning again every month. The statistics for earlier months will be stored in the `/counters` service.

```js
import { counter } from "@lpgroup/feathers-counters-service";

export default {
  after: {
    all: [],
    find: [],
    get: [],
    create: [counter("visitors", "yyyy-MM", "visits", 1, "Number of visits during a month")],
    update: [],
    patch: [],
    remove: [],
  },
};
```

## Example 4

Keep track of a stack of files, and for every `create` will increase 1 and for every delete will decrease 1.

```js
import { counter } from "@lpgroup/feathers-counters-service";

export default {
  after: {
    all: [],
    find: [],
    get: [],
    create: [counter("files", null, "files", 1, "Total number of files")],
    update: [],
    patch: [],
    remove: [counter("files", null, "files", -1, "Total number of files")],
  },
};
```

## Contribute

See [contribute](https://gitlab.com/lpgroup/lpgroup/-/blob/master/README.md#contribute)

## License

MIT - See [licence](https://gitlab.com/lpgroup/lpgroup/-/blob/master/LICENSE.md)
