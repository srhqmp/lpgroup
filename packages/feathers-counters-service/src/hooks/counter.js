/* eslint-disable no-param-reassign */
import { buildItemHook } from "@lpgroup/feathers-utils";
import { updateOrCreateCount } from "../utils/updateOrCreateCount.js";

export default (format, periodFmt, key, step = 1, description = null) => {
  return buildItemHook(async (context, item) => {
    const res = await updateOrCreateCount(context, format, periodFmt, step, description);
    item[key] = res.value || 0;
    return context;
  });
};
