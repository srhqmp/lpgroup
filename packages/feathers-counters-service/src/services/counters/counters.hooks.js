import { patchData, loadData } from "@lpgroup/feathers-mongodb-hooks/hooks";
import { url } from "@lpgroup/feathers-utils/hooks";
import { auth, changed } from "@lpgroup/feathers-auth-service/hooks";
import { validReq, validDB } from "@lpgroup/yup";

export default {
  before: {
    all: [auth()],
    find: [],
    get: [],
    create: [validReq(), changed(), validDB()],
    update: [validReq(), loadData(), changed(), validDB()],
    patch: [validReq(), patchData(), changed(), validDB()],
    remove: [],
  },

  after: {
    all: [url({ key: "alias" })],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: [],
  },
};
