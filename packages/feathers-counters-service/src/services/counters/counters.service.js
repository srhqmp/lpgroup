import { Counters } from "./counters.class.js";
import hooks from "./counters.hooks.js";
import schema from "./counters.yup.js";

export default (app) => {
  const options = {
    id: "_id",
    paginate: app.get("paginate"),
    schema,
  };
  app.use("/counters", new Counters(options, app));
  const service = app.service("counters");
  service.hooks(hooks);
};
