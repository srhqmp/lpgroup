import { Service } from "feathers-mongodb";
import { onPluginReady } from "@lpgroup/feathers-plugins";

export class Counters extends Service {
  constructor(options, app) {
    super(options);
    this.app = app;

    onPluginReady("mongodb").then(({ database }) => {
      this.Model = database.collection("counters");
    });
  }
}
