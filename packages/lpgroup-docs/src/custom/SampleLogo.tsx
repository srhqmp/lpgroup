import React from "react";
import logo from "./lpgroup.png";
import { Typography, Paper } from "@mui/material";

export const SampleLogo = () => (
  <Paper sx={{ backgroundImage: "none", boxShadow: "none" }}>
    <Typography variant="h5">@lpgroup</Typography>
  </Paper>
);
