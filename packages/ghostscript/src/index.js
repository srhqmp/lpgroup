import { log } from "@lpgroup/utils";
import { execa } from "execa";

const { debug, info, error } = log("ghostscript");

async function streamToBuffer(stream) {
  return new Promise((resolve) => {
    const bufs = [];
    stream.on("data", (d) => {
      bufs.push(d);
    });
    stream.on("end", () => {
      const buf = Buffer.concat(bufs);
      resolve(buf);
    });
  });
}

export async function optimize(opt) {
  const command = "gs";
  const { input } = opt;

  const { compressFonts = true, embedAllFonts = true, subsetFonts = true } = opt;

  const args = [
    "-sDEVICE=pdfwrite",
    "-dNOPAUSE",
    "-dQUIET",
    "-dBATCH",
    // font settings
    `-dSubsetFonts=${subsetFonts}`,
    `-dCompressFonts=${compressFonts}`,
    `-dEmbedAllFonts=${embedAllFonts}`,
    // color format
    // "-sProcessColorModel=DeviceRGB",
    // `-sColorConversionStrategy=${colorConversionStrategy}`,
    // `-sColorConversionStrategyForImages=${colorConversionStrategy}`,
    // "-dConvertCMYKImagesToRGB=true",
    // image resampling
    // "-dDetectDuplicateImages=true",
    // "-dColorImageDownsampleType=/Bicubic",
    // `-dColorImageResolution=${dpi}`,
    // "-dGrayImageDownsampleType=/Bicubic",
    // `-dGrayImageResolution=${dpi}`,
    // "-dMonoImageDownsampleType=/Bicubic",
    // `-dMonoImageResolution=${dpi}`,
    // "-dDownsampleColorImages=true",
    // other overrides
    // "-dDoThumbnails=false",
    // "-dCreateJobTicket=false",
    // "-dPreserveEPSInfo=false",
    // "-dPreserveOPIComments=false",
    // "-dPreserveOverprintSettings=false",
    // "-dUCRandBGInfo=/Remove",
    "-sstdout=%stderr",
    "-sOutputFile=-",
    "-",
  ].filter(Boolean);

  debug(`${command} ${args.join(" ")}`);
  let subprocess;
  try {
    const start = process.hrtime();
    subprocess = execa(command, args);

    subprocess.stdin.write(input);
    subprocess.stdin.end();

    const result = await streamToBuffer(subprocess.stdout);
    await subprocess;
    info(`Timer: ${process.hrtime(start)[0]} s`);
    return result;
  } catch (err) {
    error(err);
    throw new Error(`Can't optimize pdf ${err.stderr}`);
  }
}
