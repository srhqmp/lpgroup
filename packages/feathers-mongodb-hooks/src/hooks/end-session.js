import { log } from "@lpgroup/utils";
import { endSessionAndCommitTransaction, debugMsg } from "../sessions.js";

const { error } = log("database");

// eslint-disable-next-line no-unused-vars
export default (options = {}) =>
  async (context) => {
    const { params } = context;

    if (!params.sessionId) {
      error("No session started", context);
      throw Error("No session started");
    }
    debugMsg("endSession", context);
    return endSessionAndCommitTransaction(context);
  };
