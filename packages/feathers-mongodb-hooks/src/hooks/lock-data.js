import { startGetAndLockTransaction } from "../sessions.js";

// eslint-disable-next-line no-unused-vars
export default (options = {}) =>
  async (context) => {
    await startGetAndLockTransaction(context, options.collections);

    return context;
  };
