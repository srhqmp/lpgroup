import errors from "@feathersjs/errors";
import { startGetAndLockTransaction } from "../sessions.js";

export default (options = {}) =>
  async (context) => {
    const { data } = context;

    if (context.method === "update") {
      const dbData = await startGetAndLockTransaction(context, options.collections);

      if (!dbData) {
        throw new errors.NotFound(`No record found for id '${context.id}'`);
      }

      const keysToCopy = [...context.service.options.schema.getDbKeys(dbData), "_id", "alias"];
      keysToCopy.forEach((field) => {
        if (dbData[field]) data[field] = dbData[field];
      });
    }

    return context;
  };
