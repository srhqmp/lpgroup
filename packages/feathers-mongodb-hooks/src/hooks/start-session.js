import errors from "@feathersjs/errors";
import { log } from "@lpgroup/utils";
import { setClient, setDatabase, startSession, reuseSession, debugMsg } from "../sessions.js";

const { error } = log("database");

/**
 * Start a mongodb session and creates a new sessionId in params
 *
 * @param {*} options {client, database}
 */
export default (options = {}) => {
  setClient(options.client);
  setDatabase(options.database);

  return async (context) => {
    const { params } = context;

    try {
      if (params.sessionId) {
        reuseSession(params.sessionId);
        debugMsg("reuseSession", context);
      } else {
        const { sessionId, session } = await startSession();
        params.sessionId = sessionId;
        params.mongodb = { session };
        debugMsg("startSession", context);
      }
      return context;
    } catch (err) {
      error(`start-session: ${err.message}`);
      throw new errors.GeneralError(`Can't start db session`);
    }
  };
};
