import React from "react";
import { render, screen, fireEvent, renderHook } from "@testing-library/react";
import { useForm } from "react-hook-form";
import CheckboxInput from "../src/Form/CheckboxInput.tsx";
import { getById } from "./utils";

describe("<CheckboxInput />", () => {
  const { result } = renderHook(() => useForm());

  it("renders with default props", () => {
    const { container } = render(
      <CheckboxInput
        control={result.current.control}
        name="testCheckbox"
        label="Test Checkbox"
        tooltipText="Tooltip Text"
        handleValue={(value) => !value}
        disabled={false}
      />,
    );

    const checkbox = getById(container, "testCheckbox");
    const label = screen.getByText("Test Checkbox");

    expect(checkbox).toBeInTheDocument();
    expect(label).toBeInTheDocument();
  });

  it("handles checkbox state change", async () => {
    render(
      <CheckboxInput
        control={result.current.control}
        name="testCheckbox"
        label="Test Checkbox"
        tooltipText="Tooltip Text"
        disabled={false}
      />,
    );

    const checkbox = screen.getByRole("checkbox");
    fireEvent.click(checkbox);
    expect(checkbox).toBeChecked();
    fireEvent.click(checkbox);
    expect(checkbox).not.toBeChecked();
  });

  it("renders disabled checkbox when prop is disabled", () => {
    render(
      <CheckboxInput
        control={result.current.control}
        name="testCheckbox"
        label="Test Checkbox"
        tooltipText="Tooltip Text"
        disabled={true}
      />,
    );

    const checkbox = screen.getByRole("checkbox");
    expect(checkbox).toBeDisabled();
  });
});
