import React from "react";
import { render } from "@testing-library/react";
import ActionTimeStampDisplay from "../src/ActionTimestampDisplay.tsx";
import { findByTextWithRegExp } from "./utils";

const mockData = {
  added: { by: "John Doe", at: "2023-01-01T12:00:00Z" },
  changed: { by: "Jane Doe", at: "2023-01-02T14:30:00Z" },
};

describe("<ActionTimeStampDisplay />", () => {
  it("renders correctly with mock data", () => {
    render(<ActionTimeStampDisplay {...mockData} />);

    const addedText = findByTextWithRegExp(/added by John Doe at 2023-01-01T12:00:00Z/i);
    const changedText = findByTextWithRegExp(/changed by Jane Doe at 2023-01-02T14:30:00Z/i);

    expect(addedText).toBeInTheDocument();
    expect(changedText).toBeInTheDocument();
  });
});
