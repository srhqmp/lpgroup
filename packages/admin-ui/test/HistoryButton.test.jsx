import React from "react";
import { vi } from "vitest";
import { render, fireEvent, screen } from "@testing-library/react";
import HistoryButton from "../src/Buttons/HistoryButton.tsx";

describe("<HistoryButton />", () => {
  it("renders the button and calls handleHistory on click", () => {
    const mockHandleHistory = vi.fn();

    render(<HistoryButton handleHistory={mockHandleHistory} />);

    const historyButton = screen.getByText("History");
    expect(historyButton).toBeInTheDocument();

    fireEvent.click(historyButton);
    expect(mockHandleHistory).toHaveBeenCalledTimes(1);
  });

  it("renders the default tooltip text if not defined", () => {
    render(<HistoryButton handleHistory={() => {}} />);

    const tooltip = screen.getByLabelText("view changes");
    expect(tooltip).toBeInTheDocument();
  });

  it("renders the button with a tooltip", () => {
    render(<HistoryButton handleHistory={() => {}} tooltipText="Test Tooltip" />);

    const tooltip = screen.getByLabelText("Test Tooltip");
    expect(tooltip).toBeInTheDocument();
  });
});
