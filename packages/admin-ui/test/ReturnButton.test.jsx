import React from "react";
import { vi } from "vitest";
import { render, fireEvent, screen } from "@testing-library/react";
import ReturnButton from "../src/Buttons/ReturnButton.tsx";

describe("<ReturnButton />", () => {
  it("renders the button and calls handleReturn on click", () => {
    const mockHandleReturn = vi.fn();

    render(<ReturnButton handleReturn={mockHandleReturn} />);

    const returnButton = screen.getByText("Return");
    expect(returnButton).toBeInTheDocument();

    fireEvent.click(returnButton);
    expect(mockHandleReturn).toHaveBeenCalledTimes(1);
  });

  it("renders the button with a tooltip", () => {
    render(<ReturnButton handleReturn={() => {}} tooltipText="Test Tooltip" />);

    const tooltip = screen.getByLabelText("Test Tooltip");
    expect(tooltip).toBeInTheDocument();
  });
});
