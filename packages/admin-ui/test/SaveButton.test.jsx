import React from "react";
import { vi } from "vitest";
import { render, fireEvent, screen } from "@testing-library/react";
import SaveButton from "../src/Buttons/SaveButton.tsx";

describe("<SaveButton />", () => {
  it("renders the button and calls handleSave on click", () => {
    const mockHandleSave = vi.fn();

    render(<SaveButton handleSave={mockHandleSave} />);

    const saveButton = screen.getByText("Save");
    expect(saveButton).toBeInTheDocument();

    fireEvent.click(saveButton);
    expect(mockHandleSave).toHaveBeenCalledTimes(1);
  });

  it("renders the button with a tooltip", () => {
    render(<SaveButton handleSave={() => {}} tooltipText="Test Tooltip" />);

    const tooltip = screen.getByLabelText("Test Tooltip");
    expect(tooltip).toBeInTheDocument();
  });

  it("disables the button when isDisabled is true", () => {
    render(<SaveButton handleSave={() => {}} isDisabled={true} />);

    const saveButton = screen.getByText("Save");
    expect(saveButton).toBeDisabled();
  });
});
