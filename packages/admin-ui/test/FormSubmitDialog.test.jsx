import React from "react";
import { render, screen, fireEvent } from "@testing-library/react";
import { vi } from "vitest";
import FormSubmitDialog from "../src/Dialogs/FormSubmitDialog.tsx";

describe("<FormSubmitDialog />", () => {
  it("renders with default props", () => {
    render(
      <FormSubmitDialog
        title="Test Dialog"
        isOpen={true}
        handleClose={() => {}}
        handleSubmit={() => {}}
      />,
    );

    expect(screen.getByText("Test Dialog")).toBeInTheDocument();
    expect(screen.getByRole("button", { name: /submit/i })).toBeInTheDocument();
  });

  it("calls handleSubmit when the form is submitted", () => {
    const mockHandleSubmit = vi.fn().mockImplementation((e) => e.preventDefault());

    render(
      <FormSubmitDialog
        title="Test Dialog"
        isOpen={true}
        handleClose={() => {}}
        handleSubmit={mockHandleSubmit}
      />,
    );

    fireEvent.click(screen.getByRole("button", { name: /submit/i }));
    expect(mockHandleSubmit).toHaveBeenCalled();
  });

  it("calls handleClose when the dialog is closed", () => {
    const mockHandleClose = vi.fn();

    render(
      <FormSubmitDialog
        title="Test Dialog"
        isOpen={true}
        handleClose={mockHandleClose}
        handleSubmit={() => {}}
      />,
    );

    fireEvent.click(screen.getByLabelText("close"));
    expect(mockHandleClose).toHaveBeenCalled();
  });

  it("renders custom dialog text and alert components", () => {
    const customText = "Custom Dialog Text";
    const errorAlert = <div data-testid="error-alert" />;
    const successAlert = <div data-testid="success-alert" />;

    render(
      <FormSubmitDialog
        title="Test Dialog"
        isOpen={true}
        handleClose={() => {}}
        handleSubmit={() => {}}
        dialogText={customText}
        errorAlert={errorAlert}
        successAlert={successAlert}
      />,
    );

    expect(screen.getByText(customText)).toBeInTheDocument();
    expect(screen.getByTestId("error-alert")).toBeInTheDocument();
    expect(screen.getByTestId("success-alert")).toBeInTheDocument();
  });
});
