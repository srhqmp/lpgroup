import React from "react";
import { vi, done } from "vitest";
import { render, fireEvent } from "@testing-library/react";
import MenuPopover from "../src/MenuContainers/MenuPopover.tsx";

vi.useFakeTimers();

describe("<MenuPopover />", () => {
  const anchorEl = document.createElement("div");

  it("renders children when popover is set to open", () => {
    const { getByText } = render(
      <MenuPopover
        open={true}
        anchorOrigin={{ vertical: "top", horizontal: "left" }}
        transformOrigin={{ vertical: "bottom", horizontal: "right" }}
        anchorEl={anchorEl}
      >
        Test Content
      </MenuPopover>,
    );

    expect(getByText("Test Content")).toBeInTheDocument();
  });

  it("will not render anything when popover is not set to open", () => {
    const { queryByText } = render(
      <MenuPopover
        open={false}
        anchorOrigin={{ vertical: "top", horizontal: "left" }}
        transformOrigin={{ vertical: "bottom", horizontal: "right" }}
        anchorEl={anchorEl}
      >
        Test Content
      </MenuPopover>,
    );

    expect(queryByText("Test Content")).toBeNull();
  });

  it("handles onClose event when clicking outside the popover", () => {
    const { queryByText } = render(
      <MenuPopover
        open={true}
        anchorOrigin={{ vertical: "top", horizontal: "left" }}
        transformOrigin={{ vertical: "bottom", horizontal: "right" }}
        anchorEl={anchorEl}
      >
        Test Content
      </MenuPopover>,
    );

    fireEvent.click(document);
    setTimeout(() => {
      expect(queryByText("Test Content")).toBeNull();
      done();
    }, 1000);
  });
});
