import React from "react";
import { render, fireEvent, renderHook } from "@testing-library/react";
import { useForm } from "react-hook-form";
import TextInput from "../src/Form/TextInput.tsx";
import { getById } from "./utils";

describe("<TextInput />", () => {
  const { result } = renderHook(() => useForm());

  it("renders with default props", async () => {
    const { container } = render(
      <TextInput control={result.current.control} name="test-textfield" />,
    );

    const inputElement = getById(container, "test-textfield");
    expect(inputElement.value).toBe("");

    fireEvent.change(inputElement, { target: { value: "New Value" } });
    expect(inputElement.value).toBe("New Value");
  });
});
