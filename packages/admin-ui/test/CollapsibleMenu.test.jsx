import React from "react";
import { vi, done } from "vitest";
import { render, fireEvent } from "@testing-library/react";
import CollapsibleMenu from "../src/MenuContainers/CollapsibleMenu.tsx";

vi.useFakeTimers();

describe("<CollapsibleMenu />", () => {
  it("renders with default props", () => {
    const { getByText, queryByText } = render(
      <CollapsibleMenu
        title="Test Menu"
        content={<div>Test Content</div>}
        handleClick={() => {}}
        open={false}
      />,
    );

    expect(getByText("Test Menu")).toBeInTheDocument();
    expect(queryByText("Test Content")).toBeNull();
  });

  it("renders with open state and handles click", () => {
    const { getByText, queryByText } = render(
      <CollapsibleMenu
        title="Test Menu"
        content={<div>Test Content</div>}
        handleClick={() => {}}
        open={true}
      />,
    );

    expect(getByText("Test Menu")).toBeInTheDocument();
    expect(getByText("Test Content")).toBeInTheDocument();

    fireEvent.click(getByText("Test Menu"));

    setTimeout(() => {
      expect(queryByText("Test Content")).toBeNull();
      done();
    }, 1000);

    fireEvent.click(getByText("Test Menu"));
    expect(getByText("Test Content")).toBeInTheDocument();
  });

  it("renders with custom icon", () => {
    const customIcon = <span data-testid="custom-icon">Custom Icon</span>;
    const { getByTestId } = render(
      <CollapsibleMenu
        title="Test Menu"
        icon={customIcon}
        content={<div>Test Content</div>}
        handleClick={() => {}}
        open={false}
      />,
    );

    expect(getByTestId("custom-icon")).toBeInTheDocument();
  });
});
