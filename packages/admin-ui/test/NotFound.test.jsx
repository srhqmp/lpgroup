import React from "react";
import { render } from "@testing-library/react";
import NotFound from "../src/UIStates/NotFound.tsx";

describe("<NotFound />", () => {
  it("renders with given props", () => {
    const imgSrc = "path/to/image.jpg";

    const { getByText, getByAltText } = render(<NotFound imgSrc={imgSrc} />);

    expect(getByText("404")).toBeInTheDocument();
    expect(getByText(/This is not the web page[\s\S]*you are looking for\./i)).toBeInTheDocument();
    expect(getByAltText("Empty State")).toBeInTheDocument();
  });
});
