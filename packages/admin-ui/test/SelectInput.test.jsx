import React from "react";
import { render, screen, fireEvent, renderHook, within } from "@testing-library/react";
import { useForm } from "react-hook-form";
import SelectInput from "../src/Form/SelectInput.tsx";

describe("<SelectInput />", () => {
  const { result } = renderHook(() => useForm());

  const options = [
    { value: "option1", label: "Option 1" },
    { value: "option2", label: "Option 2" },
    { value: "option3", label: "Option 3" },
  ];

  it("renders with default props", () => {
    render(
      <SelectInput
        control={result.current.control}
        label="Select Label"
        name="test"
        options={options}
      />,
    );

    fireEvent.mouseDown(screen.getByRole("combobox"));
    const listbox = within(screen.getByRole("listbox"));

    options.forEach((option) => {
      expect(listbox.getByText(option.label)).toBeInTheDocument();
    });

    fireEvent.click(screen.getByText("Option 2"));
    expect(screen.getByRole("combobox")).toHaveTextContent("Option 2");
  });

  it("renders with custom props", () => {
    render(
      <SelectInput
        control={result.current.control}
        label="Custom Label"
        name="test"
        options={options}
        errorMessage="This field is required"
        error={true}
      />,
    );

    expect(screen.getAllByText("Custom Label")[0]).toBeInTheDocument();
    expect(screen.getByText("This field is required")).toBeInTheDocument();
  });
});
