import React from "react";
import { render, screen } from "@testing-library/react";
import UserAvatar from "../src/Avatars/UserAvatar.tsx";
import { getById } from "./utils";

describe("<UserAvatar />", () => {
  const mockProps = {
    name: "John Doe",
    isVisitor: false,
    onlineStatus: "ONLINE",
    isonline: true,
    size: "normal",
    imgSrc: "/path/to/image.jpg",
    isAdmin: false,
    changedAt: 1234567890,
    disableOnlineStatus: false,
  };

  it("renders UserAvatar with default props", () => {
    render(<UserAvatar name="John Doe" />);
    const avatar = screen.getByText("JD");
    expect(avatar).toBeInTheDocument();
  });

  it("renders UserAvatar with custom props", () => {
    render(<UserAvatar {...mockProps} />);
    const avatar = screen.getByAltText("Profile Image");
    expect(avatar).toBeInTheDocument();
  });

  it("renders UserAvatar with online status dot", () => {
    const { container } = render(<UserAvatar {...mockProps} />);
    const statusDot = getById(container, "online-status-dot");
    expect(statusDot).toBeInTheDocument();
  });

  it("renders UserAvatar without online status dot when disableOnlineStatus is true", () => {
    const { container } = render(<UserAvatar {...mockProps} disableOnlineStatus />);
    const statusDot = getById(container, "online-status-dot");
    expect(statusDot).toBeNull();
  });

  it("renders UserAvatar with initials when imgSrc is not provided", () => {
    render(<UserAvatar name="John Doe" />);
    const initials = screen.getByText("JD");
    expect(initials).toBeInTheDocument();
  });
});
