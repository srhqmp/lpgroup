import React, { ReactNode } from "react";
import { Box, Typography, useMediaQuery, BoxProps } from "@mui/material";

interface EmptyStateProps {
  title: string;
  subtitle: string | ReactNode;
  imgSrc: string;
  ctaButton?: ReactNode;
}

const EmptyState: React.FC<EmptyStateProps & BoxProps> = ({
  title,
  subtitle,
  imgSrc,
  ctaButton,
  ...props
}) => {
  const mobile = useMediaQuery("(max-width:800px)");

  return (
    <Box
      position="absolute"
      width="100%"
      height="100%"
      top={0}
      left={0}
      zIndex={-1}
      display="flex"
      flexDirection="column"
      alignItems="center"
      justifyContent="center"
      textAlign="center"
      {...props}
    >
      <img width={mobile ? "85%" : "45%"} src={imgSrc} alt="Empty State" />
      <Box maxWidth={300}>
        <Typography color="primary" variant="h1">
          {title}
        </Typography>
        <Typography color="primary" variant={mobile ? "caption" : "body1"} gutterBottom>
          {subtitle}
        </Typography>
        {ctaButton}
      </Box>
    </Box>
  );
};

export default EmptyState;
