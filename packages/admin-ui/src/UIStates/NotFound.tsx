import React from "react";
import EmptyState from "./EmptyState";

interface NotFoundProps {
  imgSrc: string;
}

const NotFound: React.FC<NotFoundProps> = ({ imgSrc }) => {
  return (
    <EmptyState
      title="404"
      subtitle={
        <>
          This is not the web page <br /> you are looking for.
        </>
      }
      imgSrc={imgSrc}
    />
  );
};

export default NotFound;
