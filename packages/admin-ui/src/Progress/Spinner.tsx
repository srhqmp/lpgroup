import React from "react";
import { Box, CircularProgress, CircularProgressProps } from "@mui/material";

interface SpinnerProps {
  size?: number | string;
}

const Spinner: React.FC<SpinnerProps & CircularProgressProps> = ({ size = 40, ...props }) => {
  return (
    <Box
      height="100%"
      width="100%"
      position="absolute"
      top={0}
      left={0}
      display="flex"
      justifyContent="center"
      alignItems="center"
    >
      <CircularProgress size={size} disableShrink {...props} />
    </Box>
  );
};

export default Spinner;
