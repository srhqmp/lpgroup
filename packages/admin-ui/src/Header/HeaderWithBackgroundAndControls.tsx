import React, { ReactNode } from "react";
import { Box, Container, Typography, Grid, useMediaQuery, useTheme } from "@mui/material";
import { blue } from "@mui/material/colors";

interface HeaderWithBackgroundAndControlsProps {
  title: string;
  controls?: ReactNode;
  backgroundColor?: string;
}

const HeaderWithBackgroundAndControls: React.FC<HeaderWithBackgroundAndControlsProps> = ({
  title = "",
  controls = null,
  backgroundColor = blue[100],
}) => {
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));

  return (
    <Box
      id="header-with-background"
      sx={{
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
        backgroundColor,
        paddingY: 12,
      }}
    >
      <Container maxWidth="lg">
        <Grid container spacing={2}>
          <Grid item md={6} xs={12}>
            <Typography variant="h4" color={blue[700]}>
              {title}
            </Typography>
          </Grid>
          <Grid
            item
            md={6}
            xs={12}
            display="flex"
            justifyContent={isMobile ? "flex-start" : "flex-end"}
          >
            {controls}
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
};

export default HeaderWithBackgroundAndControls;
