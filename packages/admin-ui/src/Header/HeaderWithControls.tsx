import React, { ReactNode } from "react";
import { Typography, Grid, useMediaQuery, useTheme } from "@mui/material";

interface HeaderWithControlsProps {
  title?: string;
  controls?: ReactNode;
}

const styles = {
  container: {
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    marginTop: 2,
    padding: "34px 0",
  },
};

const HeaderWithControls: React.FC<HeaderWithControlsProps> = ({ title = "", controls = null }) => {
  const theme = useTheme();
  const isMobile = useMediaQuery(theme.breakpoints.down("sm"));

  return (
    <div style={styles.container}>
      <Grid container spacing={2}>
        <Grid item md={6} xs={12}>
          <Typography variant="h5" color="primary">
            {title}
          </Typography>
        </Grid>
        <Grid
          item
          md={6}
          xs={12}
          display="flex"
          justifyContent={isMobile ? "flex-start" : "flex-end"}
        >
          {controls}
        </Grid>
      </Grid>
    </div>
  );
};

export default HeaderWithControls;
