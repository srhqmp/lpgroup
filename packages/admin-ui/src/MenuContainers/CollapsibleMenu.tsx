import React, { ReactNode } from "react";
import {
  Collapse,
  List,
  ListItemButton,
  ListItemText,
  ListItemIcon,
  Typography,
} from "@mui/material";
import { ExpandLess as ExpandLessIcon, ExpandMore as ExpandMoreIcon } from "@mui/icons-material";

interface CollapsibleMenuProps {
  title: string;
  icon?: ReactNode;
  content: ReactNode;
  handleClick: () => void;
  open: boolean;
  hasHeaderBackground?: boolean;
  mx?: number;
}

const CollapsibleMenu: React.FC<CollapsibleMenuProps> = ({
  title = "Menu",
  icon,
  content,
  handleClick,
  open = false,
  hasHeaderBackground,
  mx = 2,
}) => {
  const backgroundColor = hasHeaderBackground ? "#EEEEEE" : "";
  const border = hasHeaderBackground ? "1px solid #EEEEEE" : "";

  return (
    <List sx={{ width: "100%", padding: 0 }}>
      <ListItemButton onClick={handleClick} sx={{ backgroundColor, margin: 0 }}>
        {icon && <ListItemIcon>{icon}</ListItemIcon>}
        <ListItemText
          primary={
            <Typography variant="subtitle2" sx={{ wordBreak: "break-word" }}>
              {title}
            </Typography>
          }
        />
        {open ? <ExpandLessIcon /> : <ExpandMoreIcon />}
      </ListItemButton>
      <Collapse in={open} timeout="auto" unmountOnExit sx={{ border }}>
        <List component="div" sx={{ mx }}>
          {content}
        </List>
      </Collapse>
    </List>
  );
};

export default CollapsibleMenu;
