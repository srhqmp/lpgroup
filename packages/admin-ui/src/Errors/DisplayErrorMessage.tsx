import React from "react";
import { Box, Typography } from "@mui/material";
import { ErrorOutline as ErrorOutlineIcon } from "@mui/icons-material";

interface DisplayErrorMessageProps {
  status?: number;
  statusText?: string;
  message?: string;
}

const DisplayErrorMessage: React.FC<DisplayErrorMessageProps> = ({
  status = 404,
  statusText = "the requested page or resource is not available.",
  message = "",
}) => {
  const getErrorMessage = () => {
    switch (status) {
      case 405:
        return "Permission Denied";
      default:
        return `${status}: ${statusText}`;
    }
  };

  return (
    <Box
      sx={{
        paddingY: 10,
        textAlign: "center",
        fontSize: "100px",
      }}
    >
      <ErrorOutlineIcon fontSize="inherit" color="error" />
      <Box>
        <Typography variant="h6">{getErrorMessage()}</Typography>
        <Typography variant="body2">{message}</Typography>
      </Box>
    </Box>
  );
};

export default DisplayErrorMessage;
