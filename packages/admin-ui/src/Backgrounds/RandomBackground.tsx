import React, { useEffect, useState, ReactNode } from "react";
import { Box, Theme } from "@mui/material";
import Spinner from "../Progress/Spinner";

interface RandomBackgroundProps {
  children: ReactNode;
}

const RandomBackground: React.FC<RandomBackgroundProps> = ({ children }) => {
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const timerId = setTimeout(() => setLoading(false), 1000);
    return () => clearTimeout(timerId);
  }, []);

  return (
    <Box
      component="main"
      id="background-loading-indicator"
      sx={{
        position: "absolute",
        height: "100%",
        width: "100%",
        top: 0,
        left: 0,
        backgroundImage: "url(https://source.unsplash.com/random)",
        backgroundRepeat: "no-repeat",
        backgroundColor: (t: Theme) => t.palette.grey[50],
        backgroundSize: "cover",
        backgroundPosition: "center",
      }}
    >
      {loading && <Spinner />}
      {!loading && children}
    </Box>
  );
};

export default RandomBackground;
