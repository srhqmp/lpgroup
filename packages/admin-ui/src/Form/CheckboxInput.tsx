import React from "react";
import { Controller } from "react-hook-form";
import { Tooltip, FormControlLabel, Checkbox, TooltipProps } from "@mui/material";

interface CheckboxInputProps {
  control: any;
  name: string;
  tooltipText?: string | TooltipProps["title"];
  label?: string;
  handleValue?: (value: any) => boolean;
  handleTooltipText?: (value: any) => string;
  handleLabel?: (value: any) => string;
  disabled?: boolean;
}

const CheckboxInput: React.FC<CheckboxInputProps> = ({
  control,
  name,
  tooltipText = "",
  label = "",
  handleValue,
  handleTooltipText,
  handleLabel,
  disabled = false,
}) => {
  return (
    <Controller
      control={control}
      name={name}
      render={({ field: { onChange, value, ref } }) => (
        <Tooltip title={handleTooltipText ? handleTooltipText(value) : tooltipText}>
          <FormControlLabel
            control={
              <Checkbox
                id={name}
                inputRef={ref}
                disabled={disabled}
                checked={handleValue ? handleValue(value) : value || false}
                onChange={onChange}
              />
            }
            label={handleLabel ? handleLabel(value) : label}
          />
        </Tooltip>
      )}
    />
  );
};

export default CheckboxInput;
