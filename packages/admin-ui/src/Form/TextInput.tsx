import React from "react";
import { Controller } from "react-hook-form";
import { TextField, TextFieldProps } from "@mui/material";

interface TextInputProps {
  control: any;
  name: string;
  handleValue?: (value: any) => string | number;
}

const TextInput: React.FC<TextInputProps & TextFieldProps> = ({
  control,
  name,
  handleValue,
  ...field
}) => {
  return (
    <Controller
      control={control}
      name={name}
      render={({ field: { onChange, ref, value } }) => (
        <TextField
          id={name}
          onChange={onChange}
          inputRef={ref}
          value={handleValue ? handleValue(value) : value || ""}
          {...field}
        />
      )}
    />
  );
};

export default TextInput;
