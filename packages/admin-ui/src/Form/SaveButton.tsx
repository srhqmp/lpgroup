import React from "react";
import { Button, ButtonProps } from "@mui/material";
import { Save as SaveIcon } from "@mui/icons-material";

interface SaveButtonProps extends ButtonProps {
  disabled?: boolean;
}

const SaveButton: React.FC<SaveButtonProps> = ({ disabled, ...field }) => {
  return (
    <Button disabled={disabled} type="submit" endIcon={<SaveIcon fontSize="inherit" />} {...field}>
      Save
    </Button>
  );
};

export default SaveButton;
