import React from "react";
import { Controller } from "react-hook-form";
import { FormControl, InputLabel, Select, MenuItem, FormHelperText } from "@mui/material";

interface Option {
  value: string;
  label: string;
}

interface SelectInputProps {
  control: any;
  name: string;
  error?: boolean;
  errorMessage?: string;
  fullWidth?: boolean;
  margin?: "normal" | "dense" | "none";
  label?: string;
  handleValue?: (value: any) => string | number;
  handleLabel?: (value: any) => string;
  options: Option[];
  disabled?: boolean;
}

const SelectInput: React.FC<SelectInputProps> = ({
  control,
  name,
  error,
  errorMessage,
  fullWidth = false,
  margin = "normal",
  label = "",
  handleValue,
  handleLabel,
  options,
  disabled = false,
}) => {
  return (
    <Controller
      control={control}
      defaultValue=""
      name={name}
      render={({ field: { onChange, value, ref } }) => (
        <FormControl error={error} fullWidth={fullWidth} margin={margin} disabled={disabled}>
          <InputLabel>{handleLabel ? handleLabel(value) : label}</InputLabel>
          <Select
            inputProps={{
              id: name,
            }}
            inputRef={ref}
            onChange={onChange}
            value={handleValue ? handleValue(value) : value}
            label={handleLabel ? handleLabel(value) : label}
            defaultValue=""
          >
            {options.map((option) => (
              <MenuItem key={option.value} value={option.value}>
                {option.label}
              </MenuItem>
            ))}
          </Select>
          {error && <FormHelperText>{errorMessage}</FormHelperText>}
        </FormControl>
      )}
    />
  );
};

export default SelectInput;
