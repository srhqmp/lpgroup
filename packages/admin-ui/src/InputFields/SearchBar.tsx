import React from "react";
import { TextField, InputAdornment } from "@mui/material";
import { Search as SearchIcon } from "@mui/icons-material";

interface SearchBarProps {
  handleSearch: (event: React.ChangeEvent<HTMLInputElement>) => void;
  size?: "small" | "medium";
  label?: string;
  noIcon?: boolean;
  value?: string;
}

const SearchBar: React.FC<SearchBarProps> = ({
  handleSearch,
  size = "medium",
  label = "Search",
  noIcon,
  value = "",
}) => {
  const inputProps = noIcon
    ? {}
    : {
        style: { height: size === "small" ? 36 : "" },
        endAdornment: (
          <InputAdornment position="end">
            <SearchIcon fontSize="inherit" />
          </InputAdornment>
        ),
      };

  return (
    <TextField
      id="search-input"
      label={label}
      variant="outlined"
      color="secondary"
      size={size}
      onChange={handleSearch}
      defaultValue={value}
      fullWidth
      InputProps={inputProps}
    />
  );
};

export default SearchBar;
