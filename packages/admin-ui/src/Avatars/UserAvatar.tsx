import React from "react";
import { Badge, Avatar, styled } from "@mui/material";

const sizes = {
  xs: { width: 28, height: 28, fontSize: 14 },
  small: { width: 35, height: 35, fontSize: 18 },
  normal: { width: 40, height: 40, fontSize: 20 },
};

const getInitials = (name: string): string => {
  const nameArr = name.split(" ");
  return nameArr.length > 1 ? `${nameArr[0][0]}${nameArr[1][0]}` : name[0][0];
};

const color = {
  "ONLINE": "#44b700",
  "OFFLINE": "#26242c",
  "DO-NOT-DISTURB": "#ad1605",
};

const getColor = (status: "ONLINE" | "OFFLINE" | "DO-NOT-DISTURB", isonline: boolean): string =>
  isonline ? color[status] : color.OFFLINE;

const StyledBadge = styled(Badge)<{
  status: "ONLINE" | "OFFLINE" | "DO-NOT-DISTURB";
  isonline: string;
}>(({ theme, status, isonline }) => ({
  "& .MuiBadge-badge": {
    "backgroundColor": getColor(status, Boolean(isonline)),
    "color": getColor(status, Boolean(isonline)),
    "boxShadow": `0 0 0 2px ${theme.palette.background.paper}`,
    "&::after": {
      position: "absolute",
      top: 0,
      left: 0,
      width: "100%",
      height: "100%",
      borderRadius: "50%",
      animation: `${status === "ONLINE" && isonline && "ripple 1.2s infinite ease-in-out"}`,
      border: "1px solid currentColor",
      content: '""',
    },
  },
  "@keyframes ripple": {
    "0%": {
      transform: "scale(.8)",
      opacity: 1,
    },
    "100%": {
      transform: "scale(2.4)",
      opacity: 0,
    },
  },
}));

interface UserAvatarProps {
  name: string;
  isVisitor?: boolean;
  onlineStatus?: "ONLINE" | "OFFLINE" | "DO-NOT-DISTURB";
  isonline?: boolean;
  size?: "xs" | "small" | "normal";
  imgSrc?: string | null;
  isAdmin?: boolean;
  changedAt?: number;
  disableOnlineStatus?: boolean;
}

const UserAvatar: React.FC<UserAvatarProps> = ({
  name,
  isVisitor = false,
  onlineStatus = "OFFLINE",
  isonline = false,
  size = "normal",
  imgSrc,
  isAdmin = false,
  changedAt = 0,
  disableOnlineStatus = false,
}) => {
  const userAvatar = imgSrc ? (
    <Avatar alt="Profile Image" src={`${imgSrc}#nocache=${changedAt}`} sx={sizes[size]} />
  ) : (
    <Avatar sx={{ ...sizes[size], bgcolor: isVisitor ? "#94B49F" : "#DF7861" }}>
      {getInitials(name)}
    </Avatar>
  );

  if (disableOnlineStatus) return userAvatar;

  return (
    <StyledBadge
      overlap="circular"
      anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
      variant="dot"
      status={isVisitor ? "ONLINE" : onlineStatus}
      isonline={String(isAdmin || isonline)}
      id={isonline ? "online-status-dot" : undefined}
    >
      {userAvatar}
    </StyledBadge>
  );
};

export default UserAvatar;
