import React from "react";
import { Typography, TypographyProps } from "@mui/material";

interface CopyrightProps {
  siteName: string;
  color?: string;
}

const getCurrentYear = () => new Date().getFullYear();

const Copyright: React.FC<CopyrightProps & TypographyProps> = ({ siteName, color, ...props }) => (
  <Typography variant="caption" sx={{ color }} gutterBottom {...props}>
    {"Copyright © "}
    {siteName} {getCurrentYear()}
  </Typography>
);

export default Copyright;
