/* eslint-disable import/no-unresolved, no-unused-vars, import/no-extraneous-dependencies */

//
// Importing packages that are used by scripts and not used by code.
// This will disable the warning from "yarn sync-dep" and the "depcheck"
//

import e from "@testing-library/react";
import f from "@types/jest";
import g from "@typescript-eslint/parser";
import h from "eslint-config-airbnb";
import i from "eslint-config-airbnb-typescript";
import j from "eslint-plugin-eslint-comments";
import k from "eslint-plugin-html";
import l from "eslint-plugin-jsx-a11y";
import m from "eslint-plugin-promise";
import n from "eslint-plugin-react";
import o from "eslint-plugin-react-hooks";
import p from "eslint-plugin-unicorn";
import q from "jest-environment-jsdom";
import s from "nodemon";
import t from "react";
import u from "react-dom";
import v from "shx";
import x from "typescript";

import aa from "@vitejs/plugin-react";
import ab from "eslint-config-airbnb-base";
import ac from "eslint-config-prettier";
import ad from "eslint-plugin-jest";
import ae from "eslint-plugin-json-files";
import af from "eslint-plugin-markdown";
import ag from "eslint-plugin-node";
import ah from "eslint-plugin-mdx";
import ai from "yargs";
import aj from "@vitest/coverage-v8";
